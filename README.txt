Seeker API - README

Current publicly available calls:
	-getOSMRoute(JSON routeConfigOptions) 		//HTTP POST request which recieves JSON
	
	example format of input JSON:
	
		{
			"numberActivityDays":5,
			"numberRestDays":0,
			"startPoint":{"latitude":51.210930,"longitude":3.178559},
			"endPoint":{"latitude":50.565456,"longitude":5.726723},
			"difficulty_length":3,
			"difficulty_climbing":3,
			"circular_routes":false
		}


Setup:
- download latest version of Java 8 - https://java.com/en/download/
- install latest version of eclipse IDE - https://www.eclipse.org/downloads/
- download git (with git bash)
- clone the bitbucket repository
	- in your Git Bash terminal navigate to the place you want to save your project using normal linux terminal commands
	- clone the repository using: git clone https://willy1463@bitbucket.org/willy1463/seeker.git
	- move to develop branch using: git checkout origin develop
- open eclipse and import project
	- File-> Import -> Existing Maven Project
- Right click on project once it has imported and click Run Configurations -> double click Maven Build -> Give it a name then in 'Base Directory' chose the Seeker project folder -> in 'goals' write 'install' and mark the checkbox 'Skip Tests'. Run
	- If it gives an error saying something like 'Have you installed the JRE instead of the JDK'
		- In eclipse, Window -> Preferences -> Java -> Installed JRE'save
		- if yours says something like jre1.8.0... click on it 'Edit' and select the java JDK (which should have downloaded when you downloaded Java) from a path similar to C:\Program Files\Java\jdk1.8.0_211
- Hopefully now ready to run
	- right click on the seeker project Run -> Run as Java Application -> Select SeekerMainApplication
	- What this does is launch the file SeekerMainApplication which runs inside a java 'main' method - this is normally only done for desktop applications. Inside this method however the SpringBoot 'run' method is called which launches an instance of the Tomcat Web Servlet Container and deploys the SeekerMainApplication to it. This is all configured using the annotation @SpringBootApplication. 
- This should launch the console which will write some stuff then hopefully say launched on localhost:8070
- In order to Test - you will need to make a POST reuqest - 
	- you can download postman https://www.getpostman.com/
		- new
		- select 'POST' from dropdown
		- in URL bar put 'http://localhost:8070/getOSMRoute
		- in 'Body' tab paste JSON from above
		- Send
- you should see a response in the lower window as well as lots of logs in the Eclipse console

Some Useful Eclipse Things:
- ctrl+space will give you an auto-help option for all the objects/methods available to you at theat point in the code
- if you press control and click on a method it will take you to where it is defined
- if you select the method name where the method is defined and click ctrl+alt+h it will show you all the places it is used
- if you want to search for something in the whole project - Search->File


Summary:
- Java applciation using Spring Boot and Maven
- Restful API
- Dependencies: Lombok (util for minimising boilerplate code), GSon (util for working with Json objects)

Structure:
- The project is structured in the following way:

SeekerMain
---src/main/java/com.seeker.wmorten.seeker_main
	---SeekerMainApplication.java (porject entry point)
	---RouteController.java (main file where the API endpoints will be called (each API endpoint has the annotation @RequestMapping("nameOfEndpoint"))- notice the spring @RestController annotation which configures this funtionality automatically)
	---domain (where the objects we are trying to represent in real life/external API responses are modelled as java classes. These are all annotated with the lombok @Data annotation which creates 'getters/setters' and toString and equals methods)
	---service
		--- A set of classes which perform some logic - either retrieving and processing data from an external source (Google, Strava, OSM) or executing some logic (TSM)
		--- Each service is comstructed as an Interface whcih defines the functionality of the service but without implementing the logic and an implementation of that interface which contains the logic
		--- In RouteController you can see how these services (the interface parts) are 'injected' into the controller class using the Spring @Autowired annotation
	---utils (useful helper classes)
---src/main/resources
	---application.properties (contains properties for configuring some parts of the project (like which server port it will be launched on and the names of the API endpoints)
---pom.xml (file used by Maven to install and manage the dependencies used throuhgout the project - if you want to start using an external library search for it in the Maven central repository (https://mvnrepository.com/) find the latest stable version and copy the xml extract between the <dependency> tags into the <dependencies> section of the POM. Then you can use it throuhgout the project) 

Things still to do:
- set up some Unit Testing
- implement some Automatic Documentation for the API
- set up logging 
- general refactoring and making cleaner/more readable	