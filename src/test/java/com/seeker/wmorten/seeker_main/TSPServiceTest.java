package com.seeker.wmorten.seeker_main;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.seeker.wmorten.seeker_main.SeekerMainApplication;
import com.seeker.wmorten.seeker_main.domain.GenericCoordinate;
import com.seeker.wmorten.seeker_main.domain.GenericRouteSection;
import com.seeker.wmorten.seeker_main.domain.PlaceOfInterest;
import com.seeker.wmorten.seeker_main.domain.RouteConfigOptions;
import com.seeker.wmorten.seeker_main.service.ORSService;
import com.seeker.wmorten.seeker_main.service.TSPService;
import com.seeker.wmorten.seeker_main.service.TSPServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=SeekerMainApplication.class) 
public class TSPServiceTest {
	
	@MockBean
	ORSService orsService;
	
	@Autowired
	private TSPService tspService;
	
	@Before
	public void setUp() {

		List<GenericRouteSection> sections = new ArrayList<>();
		
		GenericRouteSection grs = new GenericRouteSection();
		grs.setStartCoOrd(new GenericCoordinate(51.210930, 3.178559));
		grs.setEndCoOrd(new GenericCoordinate(50.565456, 5.726723));
		grs.setEncodedPolyline("adfsgasdfgafdg");
		grs.setSectionDistance(50000);
		grs.setSectionTotalElevation(500);
		
		Mockito.when(orsService.getORSRouteWithWaypoints(Mockito.any(ArrayList.class))).thenReturn(grs);
	}
	
	@Test
	public void whenConnectTownsInRegion_thenReturnList() {
		
		PlaceOfInterest place1 = new PlaceOfInterest();
		PlaceOfInterest place2 = new PlaceOfInterest();

		RouteConfigOptions config = new RouteConfigOptions();
		
		config.setStartPoint(new GenericCoordinate(51.210930, 3.178559));
		config.setEndPoint(new GenericCoordinate(50.565456, 5.726723));
		config.setDifficulty_length(2);
		config.setDifficulty_climbing(2);
		config.setNumberActivityDays(4);
		
		place1.setLocation(new GenericCoordinate(50.8, 4.2));
		place1.setId(0);
		place1.setName("FirstPlace");
		place1.setPopulation(500000);
		
		place2.setLocation(new GenericCoordinate(50.9, 4.1));
		place2.setId(0);
		place2.setName("Second Place");
		place2.setPopulation(500000);
		
		List<PlaceOfInterest> list = new ArrayList<PlaceOfInterest>();
		list.add(place1);
		list.add(place2);
		
		List<PlaceOfInterest> returnPlaces = tspService.connectTownsInRegion(list, config);
		System.out.println("Funtion retrns: " + returnPlaces);
		assertThat(returnPlaces).isNotNull();
		
	}
}
