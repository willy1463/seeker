package com.seeker.wmorten.seeker_main;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.seeker.wmorten.seeker_main.domain.GenericCoordinate;
import com.seeker.wmorten.seeker_main.domain.GenericRouteSection;
import com.seeker.wmorten.seeker_main.domain.PlaceOfInterest;
import com.seeker.wmorten.seeker_main.domain.RouteConfigOptions;
import com.seeker.wmorten.seeker_main.service.GoogleService;
import com.seeker.wmorten.seeker_main.service.ORSService;
import com.seeker.wmorten.seeker_main.service.OSMService;
import com.seeker.wmorten.seeker_main.service.RouteBuilderService;
import com.seeker.wmorten.seeker_main.service.StravaService;
import com.seeker.wmorten.seeker_main.service.TSPService;

@RestController
@CrossOrigin(origins = {"http://localhost:19002/"}) 
public class RouteController {

	@Autowired 
	GoogleService googleService;
	
	@Autowired
	StravaService stravaService;

	@Autowired
	OSMService osmService;

	@Autowired
	TSPService tspService;
	
	@Autowired
	RouteBuilderService routeBuilderService;
	
	@Autowired
	ORSService orsService;	
	
	@RequestMapping("${endpoint.getRoute}")
	public List<GenericRouteSection> buildCombinedRoute(
			@RequestBody RouteConfigOptions routeOptions){
		
		List<GenericRouteSection> totalRoute = new ArrayList<>();
		
		
		List<PlaceOfInterest> allTownsInRegion = new ArrayList<>();
		//allTownsInRegion = orsService.getTownsInRegion(routeOptions);
		allTownsInRegion = routeBuilderService.getTownsInRegion(routeOptions);
		System.out.println("Read towns from CSV file and found within region: " + allTownsInRegion.size());
		System.out.println("And the towns are: " + allTownsInRegion);
		
		if(allTownsInRegion != null) {
		
			allTownsInRegion = tspService.prepareTownsForTSP(allTownsInRegion, routeOptions);
			System.out.println("After cleaning up list for TSP, size of list is: " + allTownsInRegion.size());
			
			//make initial route through best suited towns using TSP
			List<PlaceOfInterest> filteredTownsInOrder = null;
			filteredTownsInOrder = tspService.connectTownsInRegion(allTownsInRegion, routeOptions);
			
			if(filteredTownsInOrder != null) {

				System.out.println("After solving TSP, connected up this number of towns: " + filteredTownsInOrder.size());
				System.out.println("These towns are : " + filteredTownsInOrder);
				
				//calculate overnight stops
				List<PlaceOfInterest> overnightStopsPlaces = new ArrayList<>();
				overnightStopsPlaces = routeBuilderService.getOvernightStops(filteredTownsInOrder, routeOptions);
				
				if(overnightStopsPlaces != null) {

				System.out.println("After calculating the overnight stops,we found this many: " + overnightStopsPlaces.size());
				System.out.println("And they are: " + overnightStopsPlaces);

					//for each day
						//find points of interest (strava segments/towns)
						//create optimized route which passes through them
				
					for(int i=0; i<overnightStopsPlaces.size(); i++){
						
						GenericRouteSection dayRoute;
						GenericCoordinate previousPlaceLocation = null;
						
						if(i>0) {
							previousPlaceLocation = totalRoute.get(i-1).getEndCoOrd();				
						}
						
						long placeId = overnightStopsPlaces.get(i).getId();
						
						GenericCoordinate placeLocation = filteredTownsInOrder.stream().filter(t -> t.getId()==placeId).collect(Collectors.toList()).get(0).getLocation();
						
						if(i==0) {
							dayRoute = routeBuilderService.getDayRoute(routeOptions.getStartPoint(), placeLocation, routeOptions);
						}
						else {
							dayRoute = routeBuilderService.getDayRoute(previousPlaceLocation, placeLocation, routeOptions);
						}
						
						System.out.println("Obtained the day route for day: " + i);
						System.out.println("And the route is :" + dayRoute);
						totalRoute.add(dayRoute);
					}
					
					System.out.println("The total route is: " + convertToJson(totalRoute));		
					return totalRoute;
				}
				else {
					System.out.println("No overnight stops found");
				}
			}
			else {
				System.out.println("Couldn't connect up towns in TSP");
			}
		}
		System.out.println("Returning null....");
		return null;
	}
	
	private String convertToJson(List<GenericRouteSection> totalRoute) {
		
		Gson g = new Gson();
		JsonArray route = g.toJsonTree(totalRoute).getAsJsonArray();
				
		return route.toString();
	}

}
