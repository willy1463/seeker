package com.seeker.wmorten.seeker_main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@SpringBootApplication
@EnableRetry
public class SeekerMainApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeekerMainApplication.class, args);
	}

}
