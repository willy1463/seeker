package com.seeker.wmorten.seeker_main.domain.google;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class GoogleCoOrdinate {
	
	private BigDecimal lat;
	private BigDecimal lng;
	
	public String formatAsString() {
		
		return new StringBuilder()
				.append(lat.toString())
				.append(",")
				.append(lng.toString())
				.toString();
	}
	
}
