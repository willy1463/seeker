package com.seeker.wmorten.seeker_main.domain.google;

import lombok.Data;

@Data
public class GoogleDirectionsRoute {
	
	private GoogleBounds bounds;
	private String copyrights;
	private GoogleLeg[] legs;
	private GooglePolyline overview_polyline;
	private String summary;
	private String[] warnings;
	private Object[] waypoint_order;
		
}
