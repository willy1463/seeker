package com.seeker.wmorten.seeker_main.domain;

import lombok.Data;

@Data
public class RouteConfigOptions {
	
	//CONSTANTS////////////////////////////////////////////////////////////////////////////////////////////////

	//Total meters to travel each day {ideal distance per day, min distance per day, max distance per day}
	public static final int[] CONFIG_LENGTH_EASY = {35000, 20000, 50000};
	public static final int[] CONFIG_LENGTH_MED = {70000, 40000, 85000};
	public static final int[] CONFIG_LENGTH_HARD = {110000, 80000, 160000};
	
	//Array in the form of {min cat climb, max cat climb, total elevation meters over one day}
	public static final int[] CONFIG_CLIMBING_EASY = {0,1, 500};
	public static final int[] CONFIG_CLIMBING_MED = {0,3, 1000};
	public static final int[] CONFIG_CLIMBING_HARD = {0,5, 2500};
	
	//FIELDS////////////////////////////////////////////////////////////////////////////////////////////////
	
	private int numberActivityDays;
	private int numberRestDays;
	
	private GenericCoordinate startPoint; //coordinate string .eg 40.670446,-4.087570
	private GenericCoordinate endPoint;   //coordinate	string .eg 40.670446,-4.087570
	
	private int difficulty_length; // 1 - short(<35 km), 2 - medium (<70km), 3 - long (<130km)
	private int difficulty_climbing; // 1 easy (only up to cat 1) - 2 med (up to cat 3) 3 - hard (up to cat 5)
	
	private boolean circular_routes;
	
	/*
	 * in future add more things like
	 * - paved_roads_only
	 * - avoid_main_roads
	 * etc.....
	 */
	
	public int getDifficultyLengthOptimumValue(int difficulty_length) {
		
		switch (difficulty_length) {
		case 1:
			return CONFIG_LENGTH_EASY[0];			
		case 2:
			return CONFIG_LENGTH_MED[0];
		case 3:
			return CONFIG_LENGTH_HARD[0];
		default:
			break;
		}
		return CONFIG_LENGTH_EASY[0];
	}
	
	public int[] getDifficultyLengthBounds(int difficulty_length) {
		
		switch (difficulty_length) {
		case 1:
			return new int[]{CONFIG_LENGTH_EASY[1], CONFIG_LENGTH_EASY[2]};			
		case 2:
			return new int[]{CONFIG_LENGTH_MED[1], CONFIG_LENGTH_MED[2]};			
		case 3:
			return new int[]{CONFIG_LENGTH_HARD[1], CONFIG_LENGTH_HARD[2]};			
		default:
			break;
		}
		return new int[]{CONFIG_LENGTH_EASY[1], CONFIG_LENGTH_EASY[2]};			
	}
	
	public int getDifficultyLengthMaxValue(int difficulty_length) {
		
		switch (difficulty_length) {
		case 1:
			return CONFIG_LENGTH_EASY[2];			
		case 2:
			return CONFIG_LENGTH_MED[2];
		case 3:
			return CONFIG_LENGTH_HARD[2];
		default:
			break;
		}
		return CONFIG_LENGTH_EASY[2];
	}
	
	public int getDifficultyClimbingMaxValue(int difficulty_climbing) {
		
		switch (difficulty_climbing) {
		case 1:
			return CONFIG_CLIMBING_EASY[2];			
		case 2:
			return CONFIG_CLIMBING_MED[2];
		case 3:
			return CONFIG_CLIMBING_HARD[2];
		default:
			break;
		}
		return CONFIG_CLIMBING_EASY[2];
		
	}
	
}
