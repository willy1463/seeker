package com.seeker.wmorten.seeker_main.domain.google;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class GoogleCompound {

	private String text;
	private BigDecimal value;
	
}
