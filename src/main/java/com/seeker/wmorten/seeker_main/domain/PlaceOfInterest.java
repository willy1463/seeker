package com.seeker.wmorten.seeker_main.domain;

import lombok.Data;

import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvNumber;
import com.opencsv.bean.CsvRecurse;;

@Data
public class PlaceOfInterest {
	
	public static final int POI_RATING_MAX = 9;
	public static final int POI_RATING_MIN = 0;
	
	private long id;
	
    @CsvBindByPosition(position = 1)
	private String name;
	/*coordinate string*/
	@CsvRecurse
    private GenericCoordinate location;
	/*Score out of 10*/
	private int ranking;
	
	@CsvBindByPosition(position = 9)
	@CsvNumber(value = "#")
	private long population;
		
	public int getRanking() {
		if(this.ranking > POI_RATING_MAX || this.ranking <= POI_RATING_MIN) {
			this.ranking = calculateRankingFromPopulation(population);
		}
		return this.ranking;
	}

	private int calculateRankingFromPopulation(long population2) {

		if(population2 > 0) {
			switch (getRange(population2)) {
			case 1:
				return POI_RATING_MAX;
			case 2:
				return POI_RATING_MAX -2;
			case 3:
				return POI_RATING_MIN +2;
			case 4:
				return POI_RATING_MIN;
			default:
				return 5;
			}
		}
		return 5;		
	}

	private int getRange(long population2) {
		
		if(population2 > 500000) {
			return 1;
		}
		else if(population2 > 200000) {
			return 2;
		}
		else if(population2 > 50000) {
			return 3;
		}
		else return 4;
	}

}
