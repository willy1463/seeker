package com.seeker.wmorten.seeker_main.domain.google;

import lombok.Data;

@Data
public class GooglePolyline {
	
	private String points;

}
