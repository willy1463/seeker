package com.seeker.wmorten.seeker_main.domain;

import java.util.List;

import lombok.Data;

@Data
public class GenericRouteSection {

	public final static String SECTION_SOURCE_GOOGLE_MAPS = "GO";
	public final static String SECTION_SOURCE_STRAVA = "ST";
	public final static String SECTION_SOURCE_OSM = "OM";
	public final static String SECTION_SOURCE_ORS = "OR";

	
	/*Position (index) of section within total route*/
	private int order;
	/*Source of data for route info*/
	private String sectionSource;
	/*Coordinate of section start point*/
	private GenericCoordinate startCoOrd;
	/*Coordinate of section end point*/
	private GenericCoordinate endCoOrd;
	/*Encoded Polyline for route*/
	private String encodedPolyline;
	/*Distance in m of section*/
	private double sectionDistance;
	
	/*Decoded Polyline for route*/
	private String decodedPolyline;
	/*Full API response - varies depending on source*/
	private String fullResponse;
	/*Altitude of start coordinate*/
	private int startAltitude;
	/*Altitude of end coordinate*/
	private int endAltitude;
	/*Total elevation gain over route section*/
	private int sectionTotalElevation;
	
	/*List of places of interest encountered in Route Section*/
	private List<PlaceOfInterest> placesOnRoute;
	
	public GenericRouteSection(int order, String sectionSource, GenericCoordinate startCoOrd, GenericCoordinate endCoOrd, String encodedPolyline, double sectionDistance) {
		super();
		this.order = order;
		this.sectionSource = sectionSource;
		this.startCoOrd = startCoOrd;
		this.endCoOrd = endCoOrd;
		this.encodedPolyline = encodedPolyline;
		this.sectionDistance = sectionDistance;

	}

	public GenericRouteSection() {
		super();
	}
}
