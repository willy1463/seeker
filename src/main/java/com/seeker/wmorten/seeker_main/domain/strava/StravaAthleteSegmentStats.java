package com.seeker.wmorten.seeker_main.domain.strava;

import lombok.Data;

@Data
public class StravaAthleteSegmentStats {

	private int pr_elapsed_time;
	private String pr_date;
	private int effort_count;   

}
