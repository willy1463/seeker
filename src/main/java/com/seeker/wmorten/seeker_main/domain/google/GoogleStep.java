package com.seeker.wmorten.seeker_main.domain.google;

import lombok.Data;

@Data
public class GoogleStep {

	private GoogleCompound distance;
	private GoogleCompound duration;
	private GoogleCoOrdinate end_location;
	private GoogleCoOrdinate start_location;
	private String html_instructions;
	private GooglePolyline points;
	private String travel_mode;
	
}
