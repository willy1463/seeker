package com.seeker.wmorten.seeker_main.domain.google;

import lombok.Data;

@Data
public class GoogleLeg {

	private GoogleCompound distance;
	private GoogleCompound duration;
	private String end_address;
	private GoogleCoOrdinate end_location;
	private String start_address;
	private GoogleCoOrdinate start_location;
	private GoogleStep[] steps;
	private Object[] traffic_speed_entry;
	private Object[] via_waypoint;
	
}
