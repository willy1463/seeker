package com.seeker.wmorten.seeker_main.domain.strava;

import lombok.Data;

@Data
public class StravaMap {

    private String id;
    private String polyline;
    private int resource_state;
	
}
