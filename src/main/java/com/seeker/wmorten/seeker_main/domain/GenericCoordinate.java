package com.seeker.wmorten.seeker_main.domain;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvNumber;

import lombok.Data;

@Data
public class GenericCoordinate {
	
	@CsvBindByPosition(position = 2, locale = "en-GB") //, locale = "en-US"
	@CsvNumber(value = "")
	private Double latitude;
	@CsvBindByPosition(position = 3, locale = "en-GB")
	@CsvNumber("##0.00000")
	private Double longitude;
	private String coordinateString;
	private String reverseCoordinateString;
	
	public Double getLatitude() {
		
		DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
        DecimalFormat df = new DecimalFormat("##0.00000", symbols);
		df.format(this.latitude);
		return latitude;
	}
	public Double getLongitude() {
		
		DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
        DecimalFormat df = new DecimalFormat("##0.00000", symbols);
		df.format(this.longitude);
		return longitude;
	}
	
	public String getCoordinateString() {
		coordinateString = new StringBuilder().append(latitude).append(",").append(longitude).toString();
		return coordinateString;
	}

	public String getReverseCoordinateString() {
		reverseCoordinateString = new StringBuilder().append(longitude).append(",").append(latitude).toString();
		return reverseCoordinateString;
	}
	
	public GenericCoordinate(double lat, double lng) {
		this.latitude = lat;
		this.longitude = lng;
	}

	public GenericCoordinate() {
	}	
}
