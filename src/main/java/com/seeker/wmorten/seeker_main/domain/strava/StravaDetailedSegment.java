package com.seeker.wmorten.seeker_main.domain.strava;

import java.math.BigDecimal;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class StravaDetailedSegment {
	
	private long id;
	private int resource_state;
	private String name;
	private String activity_type;
	private double distance;
	private double average_grade;
	private double maximum_grade;
	private double elevation_high;
	private double elevation_low;
	private BigDecimal[] start_latlng;
	private BigDecimal[] end_latlng;
	private BigDecimal start_latitude;
	private BigDecimal start_longitude;
	private BigDecimal end_latitude;
	private BigDecimal end_longitude;
	private int climb_category;
	private String city;
	private String state;
	private String country;
	@SerializedName("private")
	private boolean isPrivate;
	private boolean hazardous;
	private boolean starred;
	private String created_at;
	private String updated_at;
	private double total_elevation_gain;
	private StravaMap map;
	private long effort_count;
	private long athlete_count;
	private long star_count;
	private StravaAthleteSegmentStats athlete_segment_stats;


}
