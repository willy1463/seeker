package com.seeker.wmorten.seeker_main.domain.strava;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class StravaSegment {
	
	private long id;
	private int resource_state;
	private String name;
	private int climb_category;
	private String climb_category_desc;
	private double avg_grade;
	private BigDecimal[] start_latlng;
	private BigDecimal[] end_latlng;
	private double elev_difference;
	private double distance;
	private String points;
	private boolean starred;
								 

}
