package com.seeker.wmorten.seeker_main.domain.google;

import lombok.Data;

@Data
public class GoogleBounds {
	
	private GoogleCoOrdinate northeast;
	private GoogleCoOrdinate southwest;
	
}
