package com.seeker.wmorten.seeker_main.service;

import java.util.List;

import com.seeker.wmorten.seeker_main.domain.GenericCoordinate;
import com.seeker.wmorten.seeker_main.domain.GenericRouteSection;
import com.seeker.wmorten.seeker_main.domain.PlaceOfInterest;
import com.seeker.wmorten.seeker_main.domain.RouteConfigOptions;

public interface RouteBuilderService {

	List<PlaceOfInterest> getOvernightStops(List<PlaceOfInterest> filteredTownsInOrder, RouteConfigOptions routeOptions);

	GenericRouteSection getDayRoute(GenericCoordinate startPoint, GenericCoordinate placeLocation, RouteConfigOptions routeOptions);

	List<PlaceOfInterest> getTownsInRegion(RouteConfigOptions routeOptions);

	GenericRouteSection checkApproxRouteDistance(List<GenericRouteSection> sections);
	
}
