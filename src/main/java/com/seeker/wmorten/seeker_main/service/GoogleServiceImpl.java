package com.seeker.wmorten.seeker_main.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

@Service
public class GoogleServiceImpl implements GoogleService {

	private static final String API_KEY = "AIzaSyAWfn3PD6ETAD1L2rb79I2TercenXK5dRo";
	private static final String GOOGLE_MAPS_API_KEY = "key";
 
	private static final String url_divider = "/";
	private static final String url_parameter_start = "?";
	private static final String url_parameter_and = "&";
	private static final String url_equals = "=";
	
	private static final String GOOGLE_MAPS_ORIGIN = "origin";
	private static final String GOOGLE_MAPS_DESTINATION = "destination";
	private static final String GOOGLE_MAPS_WAYPOINTS = "waypoints";
	private static final String GOOGLE_MAPS_VIA = "via:";
	private static final String GOOGLE_MAPS_ALTERNATIVES = "alternatives"; //takes boolean
	private static final String GOOGLE_MAPS_DEPARTURE_TIME = "departure_time";
	private static final String GOOGLE_MAPS_RESTRICTIONS = "restrictions";
	private static final String GOOGLE_MAPS_TRAVEL_MODE = "travel_mode";
	private static final String GOOGLE_MAPS_AVOID_TOLLS_MOTORWAYS = "avoid=tolls|highways";
	private static final String GOOGLE_MAPS_TRAVEL_MODE_DRIVE = "driving";
	private static final String GOOGLE_MAPS_TRAVEL_MODE_BIKE = "bicycling";
	private static final String GOOGLE_MAPS_TRAVEL_MODE_WALK = "walking";
	private static final String GOOGLE_MAPS_TRAVEL_MODE_PUBLIC_TRANSPORT = "transit";
	
	
	public ResponseEntity<String> getStepsFromAToB(@RequestParam("origin") String startPoint, @RequestParam("destination") String endPoint) {
		
//		String origin="41.43206,-81.38992";
//		String end="41.48206,-81.38892";		
//      example url: 
//		https://maps.googleapis.com/maps/api/directions/json?
//		origin=Boston,MA&destination=Concord,MA
//				&waypoints=via:Charlestown,MA|via:Lexington,MA
//				&departure_time=now
//				&key=YOUR_API_KEY
		
// 		alternatives=true can return a selection of possible routes

		
		String google_url = "https://maps.googleapis.com/maps/api/directions/";		
		String returnType = "json";		
		
		StringBuilder urlBuilder = new StringBuilder()
				.append(google_url)
				.append(returnType)
				.append(url_parameter_start)
				.append(GOOGLE_MAPS_ORIGIN)
				.append(url_equals)
				.append(startPoint)
				.append(url_parameter_and)
				.append(GOOGLE_MAPS_DESTINATION)
				.append(url_equals)
				.append(endPoint)
				.append(url_parameter_and)
				.append(GOOGLE_MAPS_TRAVEL_MODE)
				.append(url_equals)
				.append(GOOGLE_MAPS_TRAVEL_MODE_BIKE)
				.append(url_parameter_and)
				.append(GOOGLE_MAPS_RESTRICTIONS)
				.append(url_equals)
				.append(GOOGLE_MAPS_AVOID_TOLLS_MOTORWAYS)
				.append(url_parameter_and)
				.append(GOOGLE_MAPS_API_KEY)
				.append(url_equals)
				.append(API_KEY);
		
		String url = urlBuilder.toString();
		System.out.println(url);
		
		ResponseEntity<String> entity = ServiceUtils.sendHttpGetRequest(url, null);
		
		return  entity;
		
	}
	
	
}
