package com.seeker.wmorten.seeker_main.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.seeker.wmorten.seeker_main.domain.GenericCoordinate;
import com.seeker.wmorten.seeker_main.domain.RouteConfigOptions;
import com.seeker.wmorten.seeker_main.domain.strava.StravaSegment;

public interface StravaService {

	
	ResponseEntity<String> getSegments(String bounds, RouteConfigOptions options);

	List<StravaSegment> removeUnwantedSegments(List<StravaSegment> segments, GenericCoordinate startPoint, GenericCoordinate endPoint, RouteConfigOptions routeConfig);

	List<StravaSegment> filterSegmentsByProperties(List<StravaSegment> selectedSegments, RouteConfigOptions routeConfig);

	
}
