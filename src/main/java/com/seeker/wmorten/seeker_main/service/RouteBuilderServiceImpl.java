package com.seeker.wmorten.seeker_main.service;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.seeker.wmorten.seeker_main.domain.GenericCoordinate;
import com.seeker.wmorten.seeker_main.domain.GenericRouteSection;
import com.seeker.wmorten.seeker_main.domain.PlaceOfInterest;
import com.seeker.wmorten.seeker_main.domain.RouteConfigOptions;
import com.seeker.wmorten.seeker_main.domain.strava.StravaSegment;
import com.seeker.wmorten.seeker_main.utils.GeometryUtils;
import com.seeker.wmorten.seeker_main.utils.Point;

@Service
public class RouteBuilderServiceImpl implements RouteBuilderService {

	@Autowired
	StravaService stravaService;
	
	@Autowired
	ORSService orsService;	
	
	@Autowired
	DaoService daoService;
	
	private static final int MAX_ATTEMPTS_ALLOWED = 3;

	@Override
	public List<PlaceOfInterest> getOvernightStops(List<PlaceOfInterest> filteredTownsInOrder, RouteConfigOptions routeOptions) {
		
		List<PlaceOfInterest> overnightStops = new ArrayList<>();
		
		for(int i=0; i<routeOptions.getNumberActivityDays(); i++) {
			
			PlaceOfInterest stop = getOvernightStop(filteredTownsInOrder, i, overnightStops, routeOptions);
			
			if(stop != null) {
				
				overnightStops.add(stop);
			}
			else {
				
				System.out.println("We'll return the route up until the point it wasnt possible to continue");
				return overnightStops;
			}
		}
		
		return overnightStops;

	}

	private PlaceOfInterest getOvernightStop(List<PlaceOfInterest> filteredTownsInOrder, int stopIndex, List<PlaceOfInterest> overnightStops, RouteConfigOptions routeOptions) {
		
		PlaceOfInterest stop = null;
		
		GenericCoordinate dayStartPoint = (stopIndex == 0) ? 
				routeOptions.getStartPoint() :
				overnightStops.get(stopIndex-1).getLocation();
				
		int indexOfLatestStopInTownsList = overnightStops.isEmpty() ?
				0 :
				filteredTownsInOrder.indexOf(overnightStops.get(stopIndex-1));
		
		if(indexOfLatestStopInTownsList == filteredTownsInOrder.size()-1) {
			//We've used all the towns in our list in the previous days so we'll return the route up until this point
			return null;
		}
		
		int indexToContinueSearchFrom = indexOfLatestStopInTownsList+1;
		int arrayCounter = 0;
		
		for(int i=indexToContinueSearchFrom; i<filteredTownsInOrder.size(); i++) {

			String[] coordinates = new String[arrayCounter+2];
			coordinates[0]= dayStartPoint.getCoordinateString();
			
			int arrayIndex = 1;
			for(int j=indexToContinueSearchFrom; j<=i; j++) {
				coordinates[arrayIndex] = filteredTownsInOrder.get(j).getLocation().getCoordinateString();
				arrayIndex++;
			}
			
			GenericRouteSection route = orsService.getORSRoute(orsService.prepareCoordinateStringForOSM(coordinates));
			if(route== null) {
				return null;
			}
			
			if(i==indexToContinueSearchFrom && !routeWithinAllowedParams(route, routeOptions)) {
				//throw error
				System.out.println("No possible overnight stops");
				return null;
			}
			else if(!routeWithinAllowedParams(route, routeOptions)) {
				break;
			}
			else if(routeWithinAllowedParams(route, routeOptions)) {
				stop = filteredTownsInOrder.get(i);
				arrayCounter++;
			}
		}
		
		return stop;
	}

	@Override
	public GenericRouteSection getDayRoute(GenericCoordinate start, GenericCoordinate end, RouteConfigOptions routeOptions) {

		GenericCoordinate bottomLeft, topRight;

		//Strava requires rectangle defined by bottom left corner to top right corner
		switch (GeometryUtils.flipStartEndPoints(start, end)) {
		case 2:
			bottomLeft = end;
			topRight = start;
			break;
		case 3:
			bottomLeft = GeometryUtils.calculateBottomLeft(start,end);
			topRight = GeometryUtils.calculateTopRight(start,end);
			break;
		case 4:
			bottomLeft = GeometryUtils.calculateBottomLeft(end,start);
			topRight = GeometryUtils.calculateTopRight(end,start);
			break;
		// case 1 is default	
		default:
			bottomLeft = start;
			topRight = end;
			break;
		}
		
		GenericRouteSection route = new GenericRouteSection();
		
		ResponseEntity<String> segmentsJson =  stravaService.getSegments(bottomLeft.getCoordinateString()+","+topRight.getCoordinateString(), routeOptions);
		String body = segmentsJson.getBody();
		
		if(body != null) {
			
			List<StravaSegment>  segments = prepareSegmentsAsList(body);
			
			System.out.println("Segments (before)"+segments.toString());
			List<StravaSegment> selectedSegments = stravaService.removeUnwantedSegments(segments, start, end, routeOptions);
			System.out.println("Selected Segments after: " + selectedSegments.toString());
			
			if(selectedSegments != null && !selectedSegments.isEmpty()) {
								
				int numAttempts=0;
				
				while(numAttempts <= MAX_ATTEMPTS_ALLOWED && (!routeWithinAllowedParams(route, routeOptions))) {
					
					numAttempts++;

					System.out.println("Attempt no. "+numAttempts);
					System.out.println("selected segments list size: " + selectedSegments.size());
					
					if(numAttempts > 0) {
						selectedSegments = stravaService.filterSegmentsByProperties(selectedSegments, routeOptions);
					}
					
					if(selectedSegments != null && !selectedSegments.isEmpty()) {
						boolean isSegmentFirstLeg = isSegmentFirstLeg(bottomLeft, selectedSegments.get(0));
						boolean isSegmentLastLeg = isSegmentLastLeg(bottomLeft, selectedSegments.get(0));
					}
					else {
						selectedSegments = new ArrayList<StravaSegment>();
					}
					
					String coordinates = mapStravaSegmentsCoordinatesToReverseString(selectedSegments, start, end);
					
					route = orsService.getORSRoute(coordinates);
				}
				
				if(routeWithinAllowedParams(route, routeOptions)) {
					return route;
				}
			}
		}
			
		//If there are no strava segments we return the route as calculated by OSM
		return getRouteWithoutStravaSegments(start, end);
		
	}
	
	@Override
	public List<PlaceOfInterest> getTownsInRegion(RouteConfigOptions routeOptions) {
		
		double min_lat = Double.min(routeOptions.getStartPoint().getLatitude(), routeOptions.getEndPoint().getLatitude());
		double min_lng = Double.min(routeOptions.getStartPoint().getLongitude(), routeOptions.getEndPoint().getLongitude());
		double max_lat = Double.max(routeOptions.getStartPoint().getLatitude(), routeOptions.getEndPoint().getLatitude());
		double max_lng = Double.max(routeOptions.getStartPoint().getLongitude(), routeOptions.getEndPoint().getLongitude());
		
		List<PlaceOfInterest> allTowns = null;
		
		try {
			allTowns = daoService.getTowns();
		}
		catch (IllegalStateException e1) {
			e1.printStackTrace();
		}
		catch ( FileNotFoundException e2) {
			e2.printStackTrace();
		}
		
		//calculate equation of line
		//extend by 10% in either direcdtion
		//get new start/end points
		//get midpoint
		//get points perpendicular on either side

		Point originalStart = mapLocationToPoint(routeOptions.getStartPoint());
		Point originalEnd = mapLocationToPoint(routeOptions.getEndPoint());
		
		Point newStart = GeometryUtils.extendLine(originalEnd, originalStart);
		Point newEnd = GeometryUtils.extendLine(originalStart, originalEnd);

		Point newMidpoint = GeometryUtils.getMidpoint(newStart, newEnd);
		
		Point[] corners = GeometryUtils.getPerpendicularPoints(newStart, newMidpoint);
		
		Point[] orderedCorners = correctOrder(corners, newStart, newEnd);
		
		Point topCorner = orderedCorners[0];
		Point btmCorner = orderedCorners[1];	

		Point polygon1[] = {newStart, 
                topCorner,  
                newEnd,  
                btmCorner};
		
		int n = polygon1.length; 
		Set<String> duplicateFreeSet  = new HashSet<>();
		
		allTowns = allTowns
				.stream()
				.filter(t -> {
					Point p = mapLocationToPoint(t.getLocation());
					if(GeometryUtils.isInside(polygon1, n, p)) {
						return true;
					}
					return false;
				})
				.filter(t -> duplicateFreeSet.add(t.getName()))
				.collect(Collectors.toList());
		
		/*
		allTowns = allTowns
				.stream()
				.filter(t -> t.getLocation() != null && t.getLocation().getCoordinateString() != null && !t.getLocation().getCoordinateString().isEmpty())
				.filter(t -> 
					t.getLocation().getLatitude() < max_lat && t.getLocation().getLatitude() > min_lat
					&& t.getLocation().getLongitude() < max_lng && t.getLocation().getLongitude() > min_lng)
				.collect(Collectors.toList());
		*/
		System.out.println(allTowns);
		
		return allTowns;
	}
	
	private Point[] correctOrder(Point[] corners, Point newStart, Point newEnd) {
		
		double x1, x2, y1, y2;
		x1 = newStart.x;
    	y1 = newStart.y;
    	x2 = newEnd.x;
    	y2 = newEnd.y;
		
    	Point[] orderedPoints = new Point[2];
    	Point c1 = corners[0];
		Point c2 = corners[1];
		
		if(x1 < x2 && y1 < y2) {
			//normal +vs
			if(c1.x < c2.x) {
				return corners;
			}
			else {
				orderedPoints[0] = c2;
				orderedPoints[1] = c1;
				return orderedPoints;
			}
		}
		else if(x1 < x2 && y1 > y2) {
			//normal -ve
			if(c1.x > c2.x) {
				return corners;
			}
			else {
				orderedPoints[0] = c2;
				orderedPoints[1] = c1;
				return orderedPoints;
			}
		}
		else if(x1 > x2 && y1 < y2) {
			//reverse -ve
			if(c1.x < c2.x) {
				return corners;
			}
			else {
				orderedPoints[0] = c2;
				orderedPoints[1] = c1;
				return orderedPoints;
			}
		}
		else {
			//reverse +ve
			if(c1.x > c2.x) {
				return corners;
			}
			else {
				orderedPoints[0] = c2;
				orderedPoints[1] = c1;
				return orderedPoints;
			}
		}		
	}

	private Point mapLocationToPoint(GenericCoordinate coordinate) {
		
		return new Point(coordinate.getLongitude(), coordinate.getLatitude());
		
	}

	@Override
	public GenericRouteSection checkApproxRouteDistance(List<GenericRouteSection> sections) {
		
		if(sections != null && !sections.isEmpty()) {
			double sectionDistance = 0d;
			
			for(GenericRouteSection section : sections) {
				
				sectionDistance += GeometryUtils.calculateDistanceBetweenCoOrds(section.getStartCoOrd(), section.getEndCoOrd())*GeometryUtils.DETOUR_INDEX ;
			}
			
			GenericRouteSection totalAproxRoute = new GenericRouteSection();
			totalAproxRoute.setSectionDistance(sectionDistance);
					
			return totalAproxRoute;
		}
		
		return null;
	}
	
	private GenericRouteSection getRouteWithoutStravaSegments(GenericCoordinate start, GenericCoordinate end) {
				
		String initialCoordinates = new StringBuilder()
				.append(start.getReverseCoordinateString())
				.append(";")
				.append(end.getReverseCoordinateString())
				.toString();
		
		return orsService.getORSRoute(initialCoordinates);
	}

	private String mapStravaSegmentsCoordinatesToReverseString(List<StravaSegment> selectedSegments, GenericCoordinate start, GenericCoordinate end) {

		StringBuilder coordinates = new StringBuilder();
		
		coordinates
			.append(start.getReverseCoordinateString())
			.append(";");
		
		for(StravaSegment seg : selectedSegments) {
			
			coordinates
				.append(seg.getStart_latlng()[1].toPlainString())
				.append(",")
				.append(seg.getStart_latlng()[0].toPlainString())
				.append(";");		
			
			coordinates
				.append(seg.getEnd_latlng()[1].toPlainString())
				.append(",")
				.append(seg.getEnd_latlng()[0].toPlainString())
				.append(";");
		}
		
		coordinates
			.append(end.getReverseCoordinateString());
		
		return coordinates.toString();
	}

	private List<StravaSegment> prepareSegmentsAsList(String body) {
		
		List<StravaSegment> segments = new ArrayList<>();
		
		JsonObject stravaJson = new JsonParser().parse(body).getAsJsonObject();
		
		if(stravaJson != null) {
						
			if(stravaJson.has("segments") && stravaJson.get("segments").isJsonArray() && stravaJson.get("segments").getAsJsonArray().size() > 0) {
				
				Gson g = new Gson();
				
				JsonArray segsJson = stravaJson.get("segments").getAsJsonArray();
								
				for(int i=0; i<segsJson.size(); i++) {
					
					JsonObject segmentJson = segsJson.get(i).getAsJsonObject();
					System.out.println(segmentJson.toString());
					StravaSegment segment = g.fromJson(segmentJson, StravaSegment.class);
					segments.add(segment);
					
				}
			}
		}
		
		return segments;
	}
	
	private boolean routeWithinAllowedParams(GenericRouteSection route, RouteConfigOptions routeConfig) {
		
		if(route == null || route.getStartCoOrd() == null || route.getEndCoOrd() == null) {
			return false;
		}
		
		int maxLengthAllowed = routeConfig.getDifficultyLengthMaxValue(routeConfig.getDifficulty_length());
		int maxElevationAllowed = routeConfig.getDifficultyClimbingMaxValue(routeConfig.getDifficulty_climbing());
		
		double totalJourneyLength = route.getSectionDistance();
		double totalJourneyElevation = route.getSectionTotalElevation();			
		
		System.out.println("routeWithinAllowedParams, total lenght: " + totalJourneyLength);
		System.out.println("routeWithinAllowedParams, total elevation: " + totalJourneyElevation);		

		if(totalJourneyLength < maxLengthAllowed && totalJourneyElevation < maxElevationAllowed) return true;
		
		return false;
	}
	
	private boolean isSegmentFirstLeg(GenericCoordinate bottomLeft, StravaSegment stravaSegment) {
		return false;
	}
	private boolean isSegmentLastLeg(GenericCoordinate bottomLeft, StravaSegment stravaSegment) {
		return false;
	}

}
