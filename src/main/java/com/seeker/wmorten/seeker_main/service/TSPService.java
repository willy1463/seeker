package com.seeker.wmorten.seeker_main.service;

import java.util.List;

import com.seeker.wmorten.seeker_main.domain.PlaceOfInterest;
import com.seeker.wmorten.seeker_main.domain.RouteConfigOptions;

public interface TSPService {

	/**
	 * Method which solves the travelling salesman problem for the given list of towns. The problem bounds are configured
	 * by the start/end points and the route difficulty in the routeConfig as well as the location and ranking of each individual town in the list of towns.
	 * 
	 * @param townsWithWeightings
	 * @param routeConfig
	 * @return List of towns in order to which they may optimally be visited, considering the ranking as well as its location
	 */
	List<PlaceOfInterest> connectTownsInRegion(List<PlaceOfInterest> townsWithWeightings, RouteConfigOptions routeConfig);
	
	/**
	 * Method which simply checks if the trip start location is included in the list of towns - in which case it removes it 
	 * and if the trip end point is included in the list - if not it adds it - regardless we set its ranking to the Maximum value 
	 * 
	 * @param townsWithWeightings
	 * @param routeConfig
	 * @return List of towns ready to be fed into the TSP algorithm
	 */
	List<PlaceOfInterest> prepareTownsForTSP(List<PlaceOfInterest> townsWithWeightings, RouteConfigOptions routeConfig);
	
}
