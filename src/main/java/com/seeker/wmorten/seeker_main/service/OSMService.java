package com.seeker.wmorten.seeker_main.service;

import java.util.List;

import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.annotation.Backoff;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

import com.seeker.wmorten.seeker_main.domain.GenericRouteSection;

public interface OSMService {
	
	@Retryable(value = {RestClientException.class, HttpClientErrorException.class}, 
		      maxAttempts = 5,
		      backoff = @Backoff(delay = 1000))
	GenericRouteSection getOSMRoute(String coordinates);
	
	@Retryable(value = {RestClientException.class, HttpClientErrorException.class}, 
		      maxAttempts = 5,
		      backoff = @Backoff(delay = 1000))
	GenericRouteSection getOSMRouteWithWaypoints(List<GenericRouteSection> sections);
	
	String prepareCoordinateStringForOSM(String... lat_lng_pair);
	
}
