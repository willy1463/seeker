package com.seeker.wmorten.seeker_main.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.opencsv.bean.CsvToBeanBuilder;
import com.seeker.wmorten.seeker_main.domain.PlaceOfInterest;

@Service
public class DaoServiceImpl implements DaoService {

	@Value("${csv.file}")
	private String csvFilePath;
	
	@Override
	public List<PlaceOfInterest> getTowns() throws IllegalStateException, FileNotFoundException {
		
		//Bring all towns from csv file into memory
		List<PlaceOfInterest> allTowns = new ArrayList<PlaceOfInterest>();
		System.out.println(new File(csvFilePath).getAbsolutePath());
		allTowns = new CsvToBeanBuilder<PlaceOfInterest>(new FileReader(csvFilePath)).withSkipLines(1).withType(PlaceOfInterest.class).build().parse();		
		
		return allTowns;
	}

}
