package com.seeker.wmorten.seeker_main.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalField;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.seeker.wmorten.seeker_main.domain.GenericCoordinate;
import com.seeker.wmorten.seeker_main.domain.RouteConfigOptions;
import com.seeker.wmorten.seeker_main.domain.strava.StravaDetailedSegment;
import com.seeker.wmorten.seeker_main.domain.strava.StravaSegment;
import com.seeker.wmorten.seeker_main.utils.GeometryUtils;

import ch.qos.logback.core.recovery.ResilientOutputStreamBase;

@Service
public class StravaServiceImpl implements StravaService {

	private static final String url_divider = "/";
	private static final String url_parameter_start = "?";
	private static final String url_parameter_and = "&";
	private static final String url_equals = "=";
	
	private static final String STRAVA_BOUNDS = "bounds";
	private static final String STRAVA_ACTIVITY_TYPE ="activity_type";
	private static final String STRAVA_MIN_CAT = "min_cat";
	private static final String STRAVA_MAX_CAT = "max_cat";
	private static final String STRAVA_ACTIVITY_TYPE_BIKE = "riding";
	private static final String STRAVA_ACTIVITY_TYPE_RUN = "running";
	
	private static final String STRAVA_REFRESH_TOKEN = "9e036fc38e719d9714cbff902f74fb9149f514f6";
	private static final int STRAVA_CLIENT_ID = 38533;
	private static final String STRAVA_CLIENT_SECRET = "0d3f32ba4241656db1b4f15398049f09b2958fef";
	private static final String STRAVA_GRANT_TYPE_REFRESH = "refresh_token";
	private static final String STRAVA_REFRESH_RESPONSE_ACCESS_TOKEN="access_token";
	private static final double SEGMENT_LENGTH_MIN = 2000;
	private static final double MIN_SEGMENT_SEPARATION = 3000;
	
	private static String STRAVA_ACCESS_TOKEN;
	
	@RequestMapping("${endpoint.strava.segments}")
	public ResponseEntity<String> getSegments(@RequestParam("bounds") String bounds, @RequestParam("options") RouteConfigOptions options) {
		
		if(STRAVA_ACCESS_TOKEN == null || STRAVA_ACCESS_TOKEN.isEmpty()) {
			STRAVA_ACCESS_TOKEN = obtainAccessToken();
		}
		
		// http get "https://www.strava.com/api/v3/segments/explore?bounds=&activity_type=&min_cat=&max_cat=" "Authorization: Bearer [[token]]"
			
		String strava_url = "https://www.strava.com/api/v3/segments/explore";
		
		int climbMin=0, climbMax=1;
		if(options != null && options.getDifficulty_climbing() > 0) {
			switch (options.getDifficulty_climbing()) {
			case 1:
				climbMax=RouteConfigOptions.CONFIG_CLIMBING_EASY[1];
				climbMin=RouteConfigOptions.CONFIG_CLIMBING_EASY[0];
				break;
			case 2:
				climbMax=RouteConfigOptions.CONFIG_CLIMBING_MED[1];
				climbMin=RouteConfigOptions.CONFIG_CLIMBING_MED[0];
				break;
			case 3:
				climbMax=RouteConfigOptions.CONFIG_CLIMBING_HARD[1];
				climbMin=RouteConfigOptions.CONFIG_CLIMBING_HARD[0];
				break;
			default:
				break;
			}
		}
		
		StringBuilder urlBuilder = new StringBuilder()
				.append(strava_url)
				.append(url_parameter_start)
				.append(STRAVA_BOUNDS)
				.append(url_equals)
				.append(bounds)
				.append(url_parameter_and)
				.append(STRAVA_ACTIVITY_TYPE)
				.append(url_equals)
				.append(STRAVA_ACTIVITY_TYPE_BIKE)
				.append(url_parameter_and)
				.append(STRAVA_MIN_CAT)
				.append(url_equals)
				.append(climbMin)
				.append(url_parameter_and)
				.append(STRAVA_MAX_CAT)
				.append(url_equals)
				.append(climbMax);
				
		String url = urlBuilder.toString();

		Map<String, String> headers = new HashMap<String, String>();
		ResponseEntity<String> entity;
		
		try {
			headers.put(HttpHeaders.AUTHORIZATION, "Bearer " + STRAVA_ACCESS_TOKEN);
			entity = ServiceUtils.sendHttpGetRequest(url, headers);		
		}
		catch (Exception e) {
			STRAVA_ACCESS_TOKEN = obtainAccessToken();
			headers.put(HttpHeaders.AUTHORIZATION, "Bearer " + STRAVA_ACCESS_TOKEN);
			entity = ServiceUtils.sendHttpGetRequest(url, headers);	
		}
		
		if(entity!= null && entity.getBody() != null) {
			return entity;
		}
		return null;
	}
	
	@RequestMapping("${endpoint.strava.segment}")
	public ResponseEntity<String> getSegmentById(@RequestParam("id") long segmentId) {
				
		// http get "https://www.strava.com/api/v3/segments/{id}" "Authorization: Bearer [[token]]"
		
		if(STRAVA_ACCESS_TOKEN == null || STRAVA_ACCESS_TOKEN.isEmpty()) {
			STRAVA_ACCESS_TOKEN = obtainAccessToken();
		}
		
		String strava_url = "https://www.strava.com/api/v3/segments/";
		
		StringBuilder urlBuilder = new StringBuilder()
				.append(strava_url)
				.append(segmentId);
				
		String url = urlBuilder.toString();

		Map<String, String> headers = new HashMap<String, String>();
		ResponseEntity<String> entity;
		
		try {
			headers.put(HttpHeaders.AUTHORIZATION, "Bearer " + STRAVA_ACCESS_TOKEN);
			entity = ServiceUtils.sendHttpGetRequest(url, headers);		
		}
		catch (Exception e) {
			
			STRAVA_ACCESS_TOKEN = obtainAccessToken();
			headers.put(HttpHeaders.AUTHORIZATION, "Bearer " + STRAVA_ACCESS_TOKEN);
			entity = ServiceUtils.sendHttpGetRequest(url, headers);	
		}
		
		if(entity!= null && entity.getBody() != null) {
			return entity;
		}
		
		return null;
	}
		
	@Override
	public List<StravaSegment> removeUnwantedSegments(List<StravaSegment> segments, GenericCoordinate startPoint, GenericCoordinate endPoint, RouteConfigOptions options) {
		
		List<StravaSegment> orderedSelectedSegments = new ArrayList<>();
		
		if(segments != null && !segments.isEmpty()) {
			
			orderedSelectedSegments = orderByClosenessToStart(segments, startPoint);
			orderedSelectedSegments = filterBySegmentLength(orderedSelectedSegments , startPoint, endPoint, options);		
			orderedSelectedSegments = filterByElevationGain(orderedSelectedSegments , options);
			orderedSelectedSegments = removeRepeatedSegments(orderedSelectedSegments);
			
		}	
		return orderedSelectedSegments;
	}
	
	@Override
	public List<StravaSegment> filterSegmentsByProperties(List<StravaSegment> selectedSegments,	RouteConfigOptions routeConfig) {
						
		List<StravaDetailedSegment> segmentDetailsList = new ArrayList<>();

		Comparator<StravaDetailedSegment> c = Comparator
				.comparing(StravaDetailedSegment::getStar_count)
				.thenComparing(StravaDetailedSegment::getAthlete_count);
		
		Gson g = new Gson();
		
		for(int i=0; i<selectedSegments.size(); i++) {
			
			long segmentId = selectedSegments.get(i).getId();
			
			ResponseEntity<String> getSegmentResponse = getSegmentById(segmentId);
			
			if(getSegmentResponse != null && getSegmentResponse.getBody() != null) {

				JsonObject stravaJson = new JsonParser().parse(getSegmentResponse.getBody()).getAsJsonObject();
				StravaDetailedSegment segmentDetail = g.fromJson(stravaJson, StravaDetailedSegment.class);
				
				if(!segmentDetail.isHazardous() && !segmentDetail.isPrivate()) {
					segmentDetailsList.add(segmentDetail);
					System.out.println("Added segment detail list item: " + segmentDetail);
				}
			}
			else {
				System.out.println("Error getting the segment detail");
			}
		}
		
		segmentDetailsList.sort(c);
		
		//remove last element of the list when sorted using comparator
		System.out.println("filterSegmentsByProperties remove the last element with id: " + segmentDetailsList.get(0).getId());
		System.out.println("Selected segs before: " + selectedSegments);
		selectedSegments = selectedSegments
				.stream()
				.filter(s -> s.getId() != segmentDetailsList.get(0).getId())
				.collect(Collectors.toList());
		System.out.println("Selected segs after: " + selectedSegments);
		return selectedSegments;
	}	
	
	private String obtainAccessToken() {
		
		String strava_refresh_url = "https://www.strava.com/oauth/token";
		
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
		Gson g = new Gson();
		JsonObject body = new JsonObject();
		body.add("client_id", g.toJsonTree(STRAVA_CLIENT_ID)); 
		body.add("client_secret", g.toJsonTree(STRAVA_CLIENT_SECRET)); 
		body.add("grant_type", g.toJsonTree(STRAVA_GRANT_TYPE_REFRESH)); 
		body.add("refresh_token", g.toJsonTree(STRAVA_REFRESH_TOKEN)); 
		
		ResponseEntity<String> response = ServiceUtils.sendHttpPostReuqest(strava_refresh_url, headers, body);
		String new_access_token = null;

		if(response != null) {
			String responseBody  = response.getBody();
			if(responseBody != null) {
				JsonObject responseJson = new  JsonParser().parse(responseBody).getAsJsonObject();
				if(responseJson.has(STRAVA_REFRESH_RESPONSE_ACCESS_TOKEN) && !responseJson.get(STRAVA_REFRESH_RESPONSE_ACCESS_TOKEN).isJsonNull()) {
					
					new_access_token = responseJson.get(STRAVA_REFRESH_RESPONSE_ACCESS_TOKEN).getAsString();
				}
				else {
					System.out.println("Respone does NOT contain access token");
				}
			}
		}
		else {
			System.out.println("Response obtain access token is NULL");
		}
		System.out.println("Obtained new Access Token");
		return new_access_token;
	}
	
	private List<StravaSegment> orderByClosenessToStart(List<StravaSegment> segments, GenericCoordinate startPoint) {
		
		List<StravaSegment> orderedSelectedSegments = new ArrayList<>();
		
		Map<Double, Integer> distancesMap = new TreeMap<>();
		
		for(int i=0; i <segments.size(); i++) {
			
			
			double start_lat= Double.valueOf(segments.get(i).getStart_latlng()[0].toPlainString());
			double start_lng= Double.valueOf(segments.get(i).getStart_latlng()[1].toPlainString());

			GenericCoordinate segStart = new GenericCoordinate(start_lat, start_lng);
			
			double distanceToStart = GeometryUtils.calculateDistanceBetweenCoOrds(startPoint, segStart);
			
			while(distancesMap.containsKey(distanceToStart)) {
				distanceToStart+=.001;
			}
			
			distancesMap.put(distanceToStart, i);
			
		}
		
		Iterator<Entry<Double, Integer>> i = distancesMap.entrySet().iterator();
        while(i.hasNext()) {
            Map.Entry<Double, Integer> me = (Map.Entry<Double, Integer>)i.next();
            orderedSelectedSegments.add(segments.get((Integer) me.getValue()));
        }
		
		return orderedSelectedSegments;
	}
	
	private List<StravaSegment> filterBySegmentLength(List<StravaSegment> orderedSelectedSegments, GenericCoordinate startPoint, GenericCoordinate endPoint, RouteConfigOptions options) {
		
		for(int i=0; i< orderedSelectedSegments.size(); i++) {
			
			StravaSegment segment = orderedSelectedSegments.get(i);
			double segmentDistance = segment.getDistance();
			
			if(segmentDistance < SEGMENT_LENGTH_MIN) {
				orderedSelectedSegments.remove(i);
				i--;
				continue;
			}
							
			int maxLength = options.getDifficultyLengthMaxValue(options.getDifficulty_length());
						
			if((segmentDistance > maxLength)){
				orderedSelectedSegments.remove(i);
				i--;
				continue;
			}
			
			GenericCoordinate segmentStart = GeometryUtils.convertCoOrdinateArrayToString(segment.getStart_latlng());
			GenericCoordinate segmentEnd = GeometryUtils.convertCoOrdinateArrayToString(segment.getEnd_latlng());

			double journeyRoughTotal = GeometryUtils.calculateDistanceBetweenCoOrds(startPoint, segmentStart)
					+ GeometryUtils.calculateDistanceBetweenCoOrds(segmentEnd, endPoint)
					+ segmentDistance;
			
			if(journeyRoughTotal > maxLength) {
				orderedSelectedSegments.remove(i);
				i--;
				continue;
			}

		}
		
		return orderedSelectedSegments;
	}

	private List<StravaSegment> filterByElevationGain(List<StravaSegment> orderedSelectedSegments, RouteConfigOptions options) {
				
		List<StravaSegment> suitableSegments = new ArrayList<>();
		
		if(!orderedSelectedSegments.isEmpty()) {
		
			double maxElevationAllowed = options.getDifficultyClimbingMaxValue(options.getDifficulty_climbing());
						
			for(int i=0; i<orderedSelectedSegments.size(); i++) {
				
				StravaSegment segment = orderedSelectedSegments.get(i);
				
				if(segment.getElev_difference() < maxElevationAllowed) {
					suitableSegments.add(segment);
				}
			}
		}
		return suitableSegments;
	}

	private List<StravaSegment> removeRepeatedSegments(List<StravaSegment> orderedSelectedSegments) {
		
		if(orderedSelectedSegments.isEmpty()) {
			return new ArrayList<>();
		}
		else {

			List<StravaSegment> nonRepeatedSegments = new ArrayList<>();
			
			for(int i=0; i<orderedSelectedSegments.size(); i++) {
				
				List<StravaSegment> repeatedSegments = new ArrayList<>();
	
				StravaSegment segment = orderedSelectedSegments.get(i);
				boolean repeated = false;
				
				for(int j=0; j<orderedSelectedSegments.size(); j++) {
					
					StravaSegment segmentToCompare = orderedSelectedSegments.get(j);
					
					if((segment.getId() != segmentToCompare.getId()) && (
							GeometryUtils.calculateDistanceBetweenCoOrds(
							GeometryUtils.convertCoOrdinateArrayToString(segment.getStart_latlng()),
							GeometryUtils.convertCoOrdinateArrayToString(segmentToCompare.getStart_latlng())) < MIN_SEGMENT_SEPARATION)) {
					
						repeated = true;
						repeatedSegments.add(segmentToCompare);
						System.out.println("Added to repeated segments (comparing start points) segment with id: " + segmentToCompare.getId());
					}
					if((segment.getId() != segmentToCompare.getId()) && (
							GeometryUtils.calculateDistanceBetweenCoOrds(
							GeometryUtils.convertCoOrdinateArrayToString(segment.getEnd_latlng()),
							GeometryUtils.convertCoOrdinateArrayToString(segmentToCompare.getEnd_latlng())) < MIN_SEGMENT_SEPARATION)) {
					
						repeated = true;
						repeatedSegments.add(segmentToCompare);
						System.out.println("Added to repeated segments (comparing end points) segment with id: " + segmentToCompare.getId());
					}
				}
				
				if(!repeated) {				
					nonRepeatedSegments.add(segment);
				}
				else {
					
					int indexLongest = -1;
					
					for(int j=0; j<repeatedSegments.size(); j++) {
						
						if(segment.getDistance() < repeatedSegments.get(j).getDistance()) {
							indexLongest = j;
						}
					}
					
					if(indexLongest < 0) {
						nonRepeatedSegments.add(segment);
						System.out.println("Added to NONrepeated segments from else (1) segment with id: " + segment.getId());
					}
					else {
						nonRepeatedSegments.add(repeatedSegments.get(indexLongest));
						System.out.println("Added to NONrepeated segments from else (2) segment with id: " + segment.getId());
					}
				}
			}
			
			return nonRepeatedSegments;
		}
	}

}
