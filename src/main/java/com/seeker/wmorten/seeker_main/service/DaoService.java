package com.seeker.wmorten.seeker_main.service;

import java.io.FileNotFoundException;
import java.util.List;

import com.seeker.wmorten.seeker_main.domain.PlaceOfInterest;

public interface DaoService {

	/**
	 * Returns all towns from data source - currently csv file of towns
	 * @return List of all towns in csv
	 */
	List<PlaceOfInterest>getTowns() throws IllegalStateException, FileNotFoundException;
	
}
