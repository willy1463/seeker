package com.seeker.wmorten.seeker_main.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.seeker.wmorten.seeker_main.domain.GenericCoordinate;
import com.seeker.wmorten.seeker_main.domain.GenericRouteSection;
import com.seeker.wmorten.seeker_main.domain.PlaceOfInterest;
import com.seeker.wmorten.seeker_main.domain.RouteConfigOptions;

@Service
public class ORSServiceImpl implements ORSService{

	@Override
	public GenericRouteSection getORSRoute(String coordinates) {
		
		String routeStart = (coordinates.split(";")[0]);
		String routeEnd = coordinates.split(";")[coordinates.split(";").length-1];
		JsonArray coordinatesPrepared = formatCoordinatesFromString(coordinates); 
		
		ResponseEntity<String> response = getOSRRoute(coordinatesPrepared);
						
		return mapORSResponseToGenericRouteSection(response, routeStart, routeEnd);
	}

	@Override
	public GenericRouteSection getORSRouteWithWaypoints(List<GenericRouteSection> sections) {
		
		JsonArray coordinatesPrepared = formatCoordinatesFromList(sections);
		String routeStart = sections.get(0).getStartCoOrd().getCoordinateString();
		String routeEnd = sections.get(sections.size()-1).getEndCoOrd().getCoordinateString();

		ResponseEntity<String> response = getOSRRoute(coordinatesPrepared);

		return mapORSResponseToGenericRouteSection(response, routeStart, routeEnd);
	}
	
	@Override
	public List<PlaceOfInterest> getTownsInRegion(RouteConfigOptions routeOptions) {
				
		String pois_url = "https://api.openrouteservice.org/pois";
		
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    headers.add(HttpHeaders.AUTHORIZATION, getORSApiKey());
	    
	    Gson g = new Gson();
		JsonObject body = new JsonObject();
		body.add("request", g.toJsonTree("pois"));
		
		JsonObject geometryJson = new JsonObject();
		
		//geometryJson.add("bbox", value);
		
		JsonObject geoJson = new JsonObject();
		geoJson.add("type", g.toJsonTree("Point"));
		geoJson.add("coordinates", g.toJsonTree(new double[]{routeOptions.getStartPoint().getLongitude(), routeOptions.getStartPoint().getLatitude()}));
		
		geometryJson.add("geojson", geoJson);
		
		geometryJson.add("buffer", g.toJsonTree(500));
		
		body.add("geometry", geometryJson);
		
		//{"request":"pois","geometry":{"bbox":[[8.8034,53.0756],[8.7834,53.0456]],"geojson":{"type":"Point","coordinates":[8.8034,53.0756]},"buffer":200}}
		//{"request":"pois","geometry":{"geometry":{"type":"Point","coordinates":[-5.005918,41.50356]},"buffer":500}}

		ResponseEntity<String> response = null;
		
		try {
			response = ServiceUtils.sendHttpPostReuqest(pois_url, headers, body);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return mapResponseToListOfPlaces(response);
	}
	
	/**
	 * Utility method to reverse the order of a coordinate string as OSM takes the longitude part first
	 * e.g Pass in a list of strings in format {"lat1,lng1"}, {"lat2,lng2"} and return a string in format "lng1,lat1;lng2,lat2"
	 */
	@Override
	public String prepareCoordinateStringForOSM(String... lat_lng_pair) {
		
		StringBuilder coordinates = new StringBuilder();
		for(String coord : lat_lng_pair) {
			coordinates
				.append(coord.split(",")[1])
				.append(",")
				.append(coord.split(",")[0])
				.append(";");
		}
		
		return coordinates.toString().substring(0, coordinates.length()-1);
	}
	
	private ResponseEntity<String> getOSRRoute(JsonArray coordinatesPrepared) {
		
		String ors_url = "https://api.openrouteservice.org/v2/directions/driving-car"; //also availabkle cycling-road/cycling-electric

		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    headers.add(HttpHeaders.AUTHORIZATION, getORSApiKey());
		
	    Gson g = new Gson();
		JsonObject body = new JsonObject();
		body.add("coordinates", coordinatesPrepared); 
		JsonArray attributesArray = new JsonArray();
		attributesArray.add("percentage");
		body.add("attributes", attributesArray); 
		body.add("elevation", g.toJsonTree(true)); 
		//body.add("extra_info", g.toJsonTree("\"steepness\",\"suitability\",\"surface\",\"waytype\",\"tollways\"") );  //TODO
		//body.add("geometry_simplify", g.toJsonTree(false)); 
		//body.add("id", ); 
		body.add("instructions", g.toJsonTree(false)); 
		//body.add("instructions_format", ); 
		//body.add("language", ); 
		//body.add("maneuvers", );
		JsonObject options = new JsonObject();
		JsonArray avoidFeatures = new JsonArray();
		avoidFeatures.add(g.toJsonTree("highways"));
		options.add("avoid_features", avoidFeatures);
		body.add("options", options); //avoid features, avoid polygons profile params
		body.add("preference", g.toJsonTree("recommended")); 
		//body.add("radiuses", ); 
		//body.add("roundabout_exits", ); 
		//body.add("skip_segments", ); 
		body.add("suppress_warnings", g.toJsonTree(false)); 
		//body.add("units", ); default meters 
		//body.add("bearings", ); 
		//body.add("continue_straight", g.toJsonTree(true)); //only applies for cycling not driving 
		body.add("geometry", g.toJsonTree(true)); 

		ResponseEntity<String> response = ServiceUtils.sendHttpPostReuqest(ors_url, headers, body);
		
		return response;	
		
	}
	
	private GenericRouteSection mapORSResponseToGenericRouteSection(ResponseEntity<String> response, String routeStart, String routeEnd) {

		if(response != null) {
			JsonArray routes = new JsonParser().parse(response.getBody()).getAsJsonObject().get("routes").getAsJsonArray();
			JsonObject route = routes.get(0) != null ? routes.get(0).getAsJsonObject() : null;
			
			if(route != null) {
				GenericRouteSection grs = new GenericRouteSection();
				grs.setSectionSource(GenericRouteSection.SECTION_SOURCE_ORS);
				
				GenericCoordinate gcStart = new GenericCoordinate(Double.valueOf(routeStart.split(",")[1]), Double.valueOf(routeStart.split(",")[0]));
				GenericCoordinate gcEnd = new GenericCoordinate(Double.valueOf(routeEnd.split(",")[1]), Double.valueOf(routeEnd.split(",")[0]));
	
				grs.setStartCoOrd(gcStart);
				grs.setEndCoOrd(gcEnd);			
				grs.setSectionTotalElevation(route.has("summary") ?(int) route.get("summary").getAsJsonObject().get("ascent").getAsDouble(): 0);
				grs.setSectionDistance(route.has("summary") ? route.get("summary").getAsJsonObject().get("distance").getAsDouble(): 0);
				grs.setEncodedPolyline(route.has("geometry") ? route.get("geometry").getAsString(): "");
				
				return grs;
			}
		}
		return null;
	}

	private List<PlaceOfInterest> mapResponseToListOfPlaces(ResponseEntity<String> response) {
		
		List<PlaceOfInterest> placesInRegion = new ArrayList<>();
		JsonArray feautres = null;
		try {
			feautres = new JsonParser().parse(response.getBody()).getAsJsonObject().get("features").getAsJsonArray();
		}
		catch (Exception e) {
			System.out.println("Couldnt get feature list from API response");
			return null;
		}
		
		for(JsonElement f : feautres) {
			
			JsonObject feature = f.getAsJsonObject();
			
			PlaceOfInterest poi = new PlaceOfInterest();
			poi.setName(feature.get("properties").getAsJsonObject().get("osm_tags").getAsJsonObject().get("name").getAsString());
			
			double loc_lat = feature.get("geometry").getAsJsonObject().get("coordinates").getAsJsonArray().get(1).getAsDouble();
			double loc_lng = feature.get("geometry").getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsDouble();
			poi.setLocation(new GenericCoordinate(loc_lat, loc_lng));
		
			//TODO work out ranking criteria
			poi.setRanking(getPlaceRanking());
		}
		
		return null;
	}
	
	private int getPlaceRanking() {
		// TODO Auto-generated method stub
		return 5;
	}

	private String getORSApiKey() {
		
		return "5b3ce3597851110001cf624818ef294f2283453681c515bdd6c6b93b";
	}

	private JsonArray formatCoordinatesFromString(String coordinates) {
		
		JsonArray array = new JsonArray();
		
		Arrays.asList(coordinates.split(";")).forEach(c ->  {
			JsonArray coordArray = new JsonArray();
			coordArray.add(Double.valueOf(c.split(",")[0]));
			coordArray.add(Double.valueOf(c.split(",")[1]));
			array.add(coordArray);
		});
		
		return array;
	}
	
	private JsonArray formatCoordinatesFromList(List<GenericRouteSection> sections) {
		
		JsonArray array = new JsonArray();
		JsonArray initialCoordArray = new JsonArray();
		initialCoordArray.add(sections.get(0).getStartCoOrd().getLongitude());
		initialCoordArray.add(sections.get(0).getStartCoOrd().getLatitude());
		
		sections.forEach(s -> {
			JsonArray coordArray = new JsonArray();
			coordArray.add(s.getEndCoOrd().getLongitude());
			coordArray.add(s.getEndCoOrd().getLatitude());
			array.add(coordArray);
		});
		
		return array;
	}

}
