package com.seeker.wmorten.seeker_main.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import com.graphhopper.jsprit.core.algorithm.VehicleRoutingAlgorithm;
import com.graphhopper.jsprit.core.algorithm.box.Jsprit;
import com.graphhopper.jsprit.core.algorithm.termination.IterationWithoutImprovementTermination;
import com.graphhopper.jsprit.core.problem.Location;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem.FleetSize;
import com.graphhopper.jsprit.core.problem.job.Job;
import com.graphhopper.jsprit.core.problem.job.Service;
import com.graphhopper.jsprit.core.problem.solution.VehicleRoutingProblemSolution;
import com.graphhopper.jsprit.core.problem.solution.route.activity.TourActivity;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleImpl;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleType;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleTypeImpl;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleImpl.Builder;
import com.graphhopper.jsprit.core.reporting.SolutionPrinter;
import com.graphhopper.jsprit.core.util.Coordinate;
import com.graphhopper.jsprit.core.util.Solutions;
import com.graphhopper.jsprit.io.problem.VrpXMLWriter;
import com.seeker.wmorten.seeker_main.domain.GenericCoordinate;
import com.seeker.wmorten.seeker_main.domain.GenericRouteSection;
import com.seeker.wmorten.seeker_main.domain.PlaceOfInterest;
import com.seeker.wmorten.seeker_main.domain.RouteConfigOptions;
import com.seeker.wmorten.seeker_main.utils.GeometryUtils;

@org.springframework.stereotype.Service
public class TSPServiceImpl implements TSPService {
	
	private static final int MAX_RETRY = 3;
	
	@Autowired
	ORSService orsService;
	
	@Autowired
	RouteBuilderService routeBuilderService;
	
	@Override
	public List<PlaceOfInterest> connectTownsInRegion(List<PlaceOfInterest> townsWithWeightings, RouteConfigOptions routeConfig) {
		
		final double costForCoordinates = GeometryUtils.getCostPerDistanceForCoordinates(routeConfig.getStartPoint(), routeConfig.getEndPoint());		
		
		List<PlaceOfInterest> orderedPlacesOfInterest = new ArrayList<>();
		VehicleRoutingProblemSolution bestSolution = null;

		boolean routeIsValid = false;
		boolean isRetry = false;
		
		double latestArrival = routeConfig.getNumberActivityDays()*routeConfig.getDifficultyLengthOptimumValue(routeConfig.getDifficulty_length());
				
		for(int i=0; i<MAX_RETRY; i++) {
			
			VehicleTypeImpl.Builder vehicleTypeBuilder = VehicleTypeImpl.Builder.newInstance("vehicleType");
			
			VehicleType vehicleType = vehicleTypeBuilder.build();
			
			Builder vehicleBuilder = VehicleImpl.Builder.newInstance("bike");
			
			double start_lat = Double.valueOf(routeConfig.getStartPoint().getLatitude()*costForCoordinates);
			double start_lng = Double.valueOf(routeConfig.getStartPoint().getLongitude()*costForCoordinates);
			Location startLocation = Location.newInstance(start_lng, start_lat);
			vehicleBuilder.setStartLocation(startLocation);
			
			//Doesnt have to return to start point and the maximum distance possible is the number of days * max km in a single day
			vehicleBuilder
				.setReturnToDepot(false)
				.setType(vehicleType)
				.setLatestArrival(latestArrival);
			
			if(isRetry) {
				latestArrival = latestArrival*0.75;
				vehicleBuilder.setLatestArrival(latestArrival);
			}
			
			VehicleImpl vehicle = vehicleBuilder.build();
	
			List<Service> townsInRegionWeighted = getWeightedTownsInRegionList(townsWithWeightings, costForCoordinates);
			
			VehicleRoutingProblem.Builder vrpBuilder = VehicleRoutingProblem.Builder.newInstance();
			vrpBuilder.setFleetSize(FleetSize.FINITE);
			vrpBuilder.addVehicle(vehicle);
						
			vrpBuilder.addAllJobs((Collection<? extends Job>) townsInRegionWeighted);
	
			VehicleRoutingProblem problem = vrpBuilder.build();
	
			VehicleRoutingAlgorithm algorithm = Jsprit.createAlgorithm(problem);
	        algorithm.setPrematureAlgorithmTermination(new IterationWithoutImprovementTermination(1000));
	
			Collection<VehicleRoutingProblemSolution> solutions = algorithm.searchSolutions();
			
			if(!solutions.isEmpty() && !solutions.stream().findFirst().get().getRoutes().isEmpty()) {
				bestSolution = Solutions.bestOf(solutions);
				new VrpXMLWriter(problem, solutions).write("output/problem-with-solution.xml");

				SolutionPrinter.print(problem, bestSolution, SolutionPrinter.Print.VERBOSE);
				routeIsValid = checkSolutionIsValid(bestSolution, routeConfig);
				
				if(routeIsValid) {
					System.out.println("valid iteration number: " + i);
					break;
				}
			}
			isRetry = true;
		}
		
		if(routeIsValid) {
			List<TourActivity> activities = bestSolution.getRoutes().stream().findFirst().get().getTourActivities().getActivities();
			
			for(int i=0; i<activities.size(); i++) {
				
				TourActivity activity = activities.get(i);
				orderedPlacesOfInterest.add(mapTSPActivityToPlaceOfInterest(townsWithWeightings, i, activity, costForCoordinates));

			}
			
			return orderedPlacesOfInterest;
		}
		
		return null;
	}
	
	@Override
	public List<PlaceOfInterest> prepareTownsForTSP(List<PlaceOfInterest> townsWithWeightings,
			RouteConfigOptions routeConfig) {
					
		townsWithWeightings.removeIf(t -> GeometryUtils.isNear(t.getLocation(), routeConfig.getStartPoint()));
		
		boolean endPointInList = false;
		
		for(int i=0; i<townsWithWeightings.size(); i++) {
			
			PlaceOfInterest t = townsWithWeightings.get(i);
			if(GeometryUtils.isNear(t.getLocation(), routeConfig.getEndPoint())) {
				t.setRanking(PlaceOfInterest.POI_RATING_MAX);
				endPointInList = true;
			}
		}
		if(!endPointInList) {
			PlaceOfInterest endPlace = new PlaceOfInterest();
			endPlace.setLocation(routeConfig.getEndPoint());
			endPlace.setRanking(PlaceOfInterest.POI_RATING_MAX);
			endPlace.setName("Trip End Point");
			endPlace.setId(townsWithWeightings.size());
			townsWithWeightings.add(endPlace);
		}
		
		return townsWithWeightings;
		
	}
	
	private boolean checkSolutionIsValid(VehicleRoutingProblemSolution solution, RouteConfigOptions routeConfig) {
		
		//get proposed route - convert to list of GenericRouteSections
		//get calculated route from start to end via stops
		//check total distance/elevation against route config
		//return true/false
				
		List<GenericRouteSection> sections = new ArrayList<>();
		
		List<TourActivity> activities = solution.getRoutes().stream().findFirst().get().getTourActivities().getActivities();
		System.out.println("Activities size: " + activities.size());

		double costForCoordinates = GeometryUtils.getCostPerDistanceForCoordinates(routeConfig.getStartPoint(), routeConfig.getEndPoint());		
		
		for(int i=0; i<activities.size(); i++) {
			GenericRouteSection section = new GenericRouteSection();
			if(i==0) {
				section.setStartCoOrd(routeConfig.getStartPoint());
			}
			else {
				section.setStartCoOrd(sections.get(i-1).getEndCoOrd());
			}
			GenericCoordinate endLocation = new GenericCoordinate();
			endLocation.setLongitude(activities.get(i).getLocation().getCoordinate().getX()/costForCoordinates);
			endLocation.setLatitude(activities.get(i).getLocation().getCoordinate().getY()/costForCoordinates);

			section.setEndCoOrd(endLocation);
			section.setOrder(i);			
			
			sections.add(section);
		}
		
		GenericRouteSection calculatedRoute = new GenericRouteSection();
		//calculatedRoute = orsService.getORSRouteWithWaypoints(sections);
		calculatedRoute = routeBuilderService.checkApproxRouteDistance(sections);
		
		return (checkDistance(calculatedRoute, routeConfig) && checkClimbing(calculatedRoute, routeConfig));
	}

	private boolean checkClimbing(GenericRouteSection calculatedRoute, RouteConfigOptions routeConfig) {
		// TODO Check if we can reliably recover elevation
		//return calculatedRoute.getSectionTotalElevation() < routeConfig.getDifficultyClimbingValue(routeConfig.getDifficulty_climbing());

		return true;
	}

	private boolean checkDistance(GenericRouteSection calculatedRoute, RouteConfigOptions routeConfig) {	
		
		if(calculatedRoute != null) {
			return calculatedRoute.getSectionDistance() < routeConfig.getDifficultyLengthMaxValue(routeConfig.getDifficulty_length())*routeConfig.getNumberActivityDays();
		}
		else {
			return false;
		}
	}

	private PlaceOfInterest mapTSPActivityToPlaceOfInterest(List<PlaceOfInterest> townsWithWeightings, int order, TourActivity activity, double costForCoordinates) {
	
		//find town from townsWithWeightings closest to Activity location
		
		int indexOfBestFitTown = -1;
		double previousDistance = Double.MAX_VALUE;
		
		for(int i=0; i<townsWithWeightings.size(); i++) {
			
			double distance = GeometryUtils.calculateDistanceBetweenCoOrds(
					mapCoOrdToGenericCoOrd(activity.getLocation().getCoordinate(), costForCoordinates),
					townsWithWeightings.get(i).getLocation());
			
			if(distance < previousDistance) {
				previousDistance = distance;
				indexOfBestFitTown = i;
			}
		}
		
		PlaceOfInterest bestFitTown = townsWithWeightings.get(indexOfBestFitTown);
		bestFitTown.setId(order);
				
		return bestFitTown; 
	}
	
	private GenericCoordinate mapCoOrdToGenericCoOrd(Coordinate coordinate, double costForCoordinates) {
		
		GenericCoordinate gc = new GenericCoordinate();
		gc.setLatitude(coordinate.getY()/costForCoordinates);
		gc.setLongitude(coordinate.getX()/costForCoordinates);
				
		return gc;
	}
/*
	private PlaceOfInterest mapTSPActivityToPlaceOfInterest(int order, TourActivity activity, double costForCoordinates) {
		
		PlaceOfInterest placeOfInterest = new PlaceOfInterest();
		
		GenericCoordinate location = new GenericCoordinate();
		location.setLatitude(activity.getLocation().getCoordinate().getY()/costForCoordinates);
		location.setLongitude(activity.getLocation().getCoordinate().getX()/costForCoordinates);
		placeOfInterest.setLocation(location);
		placeOfInterest.setName(activity.getName());
		
		placeOfInterest.setId(order);
		System.out.println(placeOfInterest);
		return placeOfInterest;
	}
*/
	private static List<Service> getWeightedTownsInRegionList(List<PlaceOfInterest> townsWithRanking, double costForCoordinates) {
		
		List<Service> weightedDestinationList = new ArrayList<Service>();
				
		for(int i=0; i < townsWithRanking.size(); i++) {
			
			PlaceOfInterest place = townsWithRanking.get(i);					
		    
		    Service service = Service.Builder.newInstance(place.getName())
		    		.setPriority(10-place.getRanking())
					.setLocation(getLocationFromPlaceOfInterest(place, costForCoordinates))
					.build();
			weightedDestinationList.add(service);
		}
				
		return weightedDestinationList;
		
	}


	private static Location getLocationFromPlaceOfInterest(PlaceOfInterest place, double costForCoordinates) {
		
		Location townLocation = Location.newInstance(place.getLocation().getLongitude()*costForCoordinates, place.getLocation().getLatitude()*costForCoordinates);
		
		return townLocation;
	}
	
}
