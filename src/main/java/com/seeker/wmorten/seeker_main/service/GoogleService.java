package com.seeker.wmorten.seeker_main.service;

import org.springframework.http.ResponseEntity;

public interface GoogleService {
	
	ResponseEntity<String> getStepsFromAToB(String startPoint, String endPoint);


}
