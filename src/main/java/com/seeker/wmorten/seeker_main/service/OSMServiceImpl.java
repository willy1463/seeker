package com.seeker.wmorten.seeker_main.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.seeker.wmorten.seeker_main.domain.GenericCoordinate;
import com.seeker.wmorten.seeker_main.domain.GenericRouteSection;

import org.springframework.stereotype.Service;

@Service
public class OSMServiceImpl implements OSMService {
	
	private static final String url_divider = "/";
	private static final String url_parameter_start = "?";
	private static final String url_parameter_and = "&";
	private static final String url_equals = "=";
	
	private static final String OSM_ALTERNATIVES = "alternatives";
	private static final String OSM_STEPS = "steps";
	private static final String OSM_ANNOTATIONS = "annotations";
	private static final String OSM_GEOMETRIES = "geometries";
	private static final String OSM_OVERVIEW = "overview";
	private static final String OSM_CONTINUE_STRAIGHT = "continue_straight";
	private static final String OSM_WAYPOINTS = "waypoints";
	
	
	public GenericRouteSection getOSMRoute(String coordinates) {
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Couldn't pause thread - continue anyway");
		}
		
		String routeStart = (coordinates.split(";")[0]);
		String routeEnd = coordinates.split(";")[coordinates.split(";").length-1];
		
		// API info http://project-osrm.org/docs/v5.22.0/api/#route-service

		String osm_url = "http://router.project-osrm.org/route/v1/driving/"; 
		
		StringBuilder urlBuilder = new StringBuilder()
				.append(osm_url)
				.append(coordinates)
				.append(url_parameter_start)
				.append(OSM_GEOMETRIES)
				.append(url_equals)
				.append("polyline")
				.append(url_parameter_and)
				.append(OSM_OVERVIEW)
				.append(url_equals)
				.append("simplified")
				.append(url_parameter_and)
				.append(OSM_CONTINUE_STRAIGHT)
				.append(url_equals)
				.append("true");

		String url = urlBuilder.toString();
		
		ResponseEntity<String> entity;
		
		entity = ServiceUtils.sendHttpGetRequest(url, null);
		
		
		return mapOSMResponseToGenericRouteSection(entity, routeStart, routeEnd);
		
	}


	@Override
	public GenericRouteSection getOSMRouteWithWaypoints(List<GenericRouteSection> sections) {
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Couldn't pause thread - continue anyway");
		}
		
		GenericCoordinate routeStart = sections.get(0).getStartCoOrd();
		GenericCoordinate routeEnd = sections.get(sections.size()-1).getEndCoOrd();
		String coordinates = "";
		StringBuilder coordinatesBuilder = new StringBuilder();
		
		for(GenericRouteSection section : sections) {
			
			coordinatesBuilder
				.append(section.getStartCoOrd().getReverseCoordinateString())
				.append(";");
			
		}
		coordinates = coordinatesBuilder.subSequence(0,(coordinatesBuilder.length()-1)).toString();
		
		// API info http://project-osrm.org/docs/v5.22.0/api/#route-service
		//{longitude},{latitude};{longitude},{latitude}[;{longitude},{latitude} ...]
		String osm_url = "http://router.project-osrm.org/route/v1/driving/"; 
		
		StringBuilder urlBuilder = new StringBuilder()
									.append(osm_url)
									.append(coordinates)
									.append(url_parameter_start)
									.append(OSM_GEOMETRIES)
									.append(url_equals)
									.append("polyline")
									.append(url_parameter_and)
									.append(OSM_OVERVIEW)
									.append(url_equals)
									.append("simplified")
									.append(url_parameter_and)
									.append(OSM_CONTINUE_STRAIGHT)
									.append(url_equals)
									.append("true");
		
		String url = urlBuilder.toString();
		ResponseEntity<String> entity;
//		Map<String, String> headers = new HashMap<>();
//		headers.put(HttpHeaders.CACHE_CONTROL, CacheControl.maxAge(499, TimeUnit.MILLISECONDS).toString());
		
		entity = ServiceUtils.sendHttpGetRequest(url, null);
		
		return mapOSMResponseToGenericRouteSection(entity, routeStart.getCoordinateString(), routeEnd.getCoordinateString());
	}
	
	/**
	 * Utility method to reverse the order of a coordinate string as OSM takes the longitude part first
	 * e.g Pass in a list of strings in format {"lat1,lng1"}, {"lat2,lng2"} and return a string in format "lng1,lat1;lng2,lat2"
	 */
	@Override
	public String prepareCoordinateStringForOSM(String... lat_lng_pair) {
		
		StringBuilder coordinates = new StringBuilder();
		for(String coord : lat_lng_pair) {
			coordinates
				.append(coord.split(",")[1])
				.append(",")
				.append(coord.split(",")[0])
				.append(";");
		}
		
		return coordinates.toString().substring(0, coordinates.length()-1);
	}	
	
	private GenericRouteSection mapOSMResponseToGenericRouteSection(ResponseEntity<String> entity, String routeStart, String routeEnd) {
		
		GenericRouteSection route = new GenericRouteSection();
		
		if(entity != null && entity.getBody() != null) {
			
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject) parser.parse(entity.getBody());
			
			if(json.has("routes") && json.get("routes") != null) {
				JsonArray routesArray = json.get("routes").getAsJsonArray();
				if(!routesArray.isJsonNull()) {
					
					double journeyDistance = routesArray.get(0).getAsJsonObject().get("distance").getAsDouble();
					route.setSectionDistance(journeyDistance);
					
					route.setStartCoOrd(new GenericCoordinate(Double.valueOf(routeStart.split(",")[0]),Double.valueOf(routeStart.split(",")[1])));
					route.setEndCoOrd(new GenericCoordinate(Double.valueOf(routeEnd.split(",")[0]),Double.valueOf(routeEnd.split(",")[1])));
					route.setEncodedPolyline(routesArray.get(0).getAsJsonObject().get("geometry").getAsString());
					route.setSectionSource(GenericRouteSection.SECTION_SOURCE_OSM);
					//TODO try and return elevation
					
				}
			}
		}
		
		return route;
	}
}
