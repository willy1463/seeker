package com.seeker.wmorten.seeker_main.service;

import java.util.Map;

import org.springframework.web.client.HttpClientErrorException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonObject;

public class ServiceUtils {
	
	public static ResponseEntity<String> sendHttpGetRequest(String url, Map<String, String> headersMap) throws RestClientException, HttpClientErrorException {
		
		System.out.println("Url specified: " + url);
		
		RestTemplate restTemplate;
		ResponseEntity<String> response = null;
		
		restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		
		if(headersMap != null) {
			
			headersMap.keySet().forEach(key -> headers.add(key.toString(), (String)headersMap.get(key)));
			System.out.println("Added the headers: " + headers.toString());
		}
		
		HttpEntity entity = new HttpEntity(headers);
		try {
			response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
			if(response.getStatusCode() != HttpStatus.OK) {
				System.out.println("Response with status code: " + response.getStatusCode());
				System.out.println("Message: " + response.getBody());
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return  response;		
	}

	public static ResponseEntity<String> sendHttpPostReuqest(String url, HttpHeaders headers, JsonObject body) {
		
		RestTemplate template = new RestTemplate();
		HttpEntity<String> request = new HttpEntity<String>(body.toString(), headers);
		System.out.println(body.toString());
		
		ResponseEntity<String> response = null;
		
		try {
			response = template.postForEntity(url, request, String.class);
			if(response.getStatusCode() != HttpStatus.OK) {
				System.out.println("Response with status code: " + response.getStatusCode());
				System.out.println("Message: " + response.getBody());
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error making post request for url: " + url);
		}
		
		return response;
		
	}
}
