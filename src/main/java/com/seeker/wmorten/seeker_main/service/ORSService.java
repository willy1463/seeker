package com.seeker.wmorten.seeker_main.service;

import java.util.List;

import com.seeker.wmorten.seeker_main.domain.GenericRouteSection;
import com.seeker.wmorten.seeker_main.domain.PlaceOfInterest;
import com.seeker.wmorten.seeker_main.domain.RouteConfigOptions;

public interface ORSService {

	GenericRouteSection getORSRoute(String coordinates);
	
	GenericRouteSection getORSRouteWithWaypoints(List<GenericRouteSection> sections);

	List<PlaceOfInterest> getTownsInRegion(RouteConfigOptions routeOptions);

	String prepareCoordinateStringForOSM(String... lat_lng_pair);
}
