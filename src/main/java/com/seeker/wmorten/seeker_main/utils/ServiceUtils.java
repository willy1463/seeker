package com.seeker.wmorten.seeker_main.utils;

import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ServiceUtils {
	
	public static ResponseEntity<String> sendHttpGetRequest(String url, Map<String, String> headersMap) {
		
		System.out.println("Url specified: " + url);
		
		RestTemplate restTemplate;
		ResponseEntity<String> response = null;
		
		try {
			
			restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			
			if(headersMap != null) {
				
				headersMap.keySet().forEach(key -> headers.add(key.toString(), (String)headersMap.get(key)));
				System.out.println("Added the headers: " + headers.toString());
			}
			
			HttpEntity entity = new HttpEntity(headers);

			response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

		}
		catch (Exception e) {
			System.out.println("error trying to make GET request");
			System.out.println(e.getMessage());
		}
		
		return  response;		
	}
	
}
