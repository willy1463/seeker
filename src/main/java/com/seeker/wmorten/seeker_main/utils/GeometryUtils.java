package com.seeker.wmorten.seeker_main.utils;
import static java.lang.Math.*;

import java.awt.geom.Point2D;
import java.math.BigDecimal;
import java.util.List;

import com.seeker.wmorten.seeker_main.domain.GenericCoordinate;
import com.seeker.wmorten.seeker_main.domain.GenericRouteSection;
import com.seeker.wmorten.seeker_main.domain.RouteConfigOptions;

public class GeometryUtils {

    private static final double EARTH_RADIUS = 6371.01 * 1000; //meters
	
    public static final double DETOUR_INDEX = 1.417; //--- detour index = 1.417 straight line distances correlate well with road network distances using this factor. https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3835347/

	public static double calculateDistanceBetweenCoOrds(GenericCoordinate coOrdinate1, GenericCoordinate coOrdinate2) {
		
		double startPointLat = coOrdinate1.getLatitude();
		double startPointLng = coOrdinate1.getLongitude();
		double segmentStartLat = coOrdinate2.getLatitude();
		double segmentStartLng = coOrdinate2.getLongitude();
		
		double diffLongitudes = toRadians(abs(segmentStartLng - startPointLng));
		double slat = toRadians(startPointLat);
		double flat = toRadians(segmentStartLat);
		
		// haversine formula
		double diffLatitudes = toRadians(abs(segmentStartLat - startPointLat));
		double a = sin(diffLatitudes / 2) * sin(diffLatitudes / 2) + cos(slat) * cos(flat) * sin(diffLongitudes / 2) * sin(diffLongitudes / 2);
		double c = 2 * atan2(sqrt(a), sqrt(1 - a)); //angular distance in radians
		
		return EARTH_RADIUS * c;
		
	}
	
	public static GenericCoordinate convertCoOrdinateArrayToString(BigDecimal[] coOrds) {
				
		double startLatD = coOrds[0].doubleValue();
		double startLngD = coOrds[1].doubleValue();
				
		return new GenericCoordinate(startLatD, startLngD);
		
	}

	public static int flipStartEndPoints(GenericCoordinate start, GenericCoordinate end) {
		
		double start_lat = start.getLatitude();
		double start_lng = start.getLongitude();
		double end_lat = end.getLatitude();
		double end_lng = end.getLongitude();
		
		if(start_lat <= end_lat && start_lng < end_lng) {
			return 1;
		}
		else if(start_lat >= end_lat && start_lng >= end_lng) {
			return 2;
		}
		else if(start_lat <= end_lat && start_lng >= end_lng) {
			return 3;
		}
		else if(start_lat >= end_lat && start_lng <= end_lng) {
			return 4;
		}
		else return 1;
	}

	public static GenericCoordinate calculateBottomLeft(GenericCoordinate start, GenericCoordinate end) {
		
		double start_lat = start.getLatitude();
		double end_lng = end.getLongitude();
		
		return new GenericCoordinate(start_lat, end_lng);
	}

	public static GenericCoordinate calculateTopRight(GenericCoordinate start, GenericCoordinate end) {
		
		double start_lng = start.getLongitude();
		double end_lat = end.getLatitude();
		
		return new GenericCoordinate(end_lat, start_lng);				
	}

	public static boolean routeDoesRetraceSteps(List<GenericRouteSection> route) {
		
		//TODO have to decode polylines
		
		return false;
	}
	
	public static boolean isNear(GenericCoordinate location1, GenericCoordinate location2) {
		
		//within 3km of eachother???
		return calculateDistanceBetweenCoOrds(location1, location2) < 3000;
	}

	public static double getCostPerDistanceForCoordinates(GenericCoordinate startPoint, GenericCoordinate endPoint) {
		// Cost per unit distance in cartesian coordinates is 1.0
		// we need to calculate this value when using georgraphic coordinatates - whilst km/change in degree of latitude is more or less constant, this is not the case for longitude
		
		double roughTripDistance = calculateDistanceBetweenCoOrds(startPoint, endPoint);
		
		double start_lat = startPoint.getLatitude();
		double start_lng = startPoint.getLongitude();
		double end_lat = endPoint.getLatitude();
		double end_lng = endPoint.getLongitude();
		
		return (roughTripDistance)/(Math.sqrt(Math.pow((end_lat-start_lat), 2) + Math.pow((end_lng-start_lng), 2)));
		
	}
	  
    // Define Infinite (Using INT_MAX  
    // caused overflow problems) 
    final static double INF = 10000d;

	private static final int EXTEND_PERCENTAGE = 10;  //written in its percentage form ie 10 = 10%
    
    // Given three colinear points p, q, r,  
    // the function checks if point q lies 
    // on line segment 'pr' 
    public static boolean onSegment(Point p, Point q, Point r)  
    { 
        if (q.x <= Math.max(p.x, r.x) && 
            q.x >= Math.min(p.x, r.x) && 
            q.y <= Math.max(p.y, r.y) && 
            q.y >= Math.min(p.y, r.y)) 
        { 
            return true; 
        } 
        return false; 
    } 
  
    // To find orientation of ordered triplet (p, q, r). 
    // The function returns following values 
    // 0 --> p, q and r are colinear 
    // 1 --> Clockwise 
    // 2 --> Counterclockwise 
    public static int orientation(Point p, Point q, Point r)  
    { 
    	double val = (q.y - p.y) * (r.x - q.x) 
                - (q.x - p.x) * (r.y - q.y); 
  
        if (val == 0)  
        { 
            return 0; // colinear 
        } 
        return (val > 0) ? 1 : 2; // clock or counterclock wise 
    } 
  
    // The function that returns true if  
    // line segment 'p1q1' and 'p2q2' intersect. 
    public static boolean doIntersect(Point p1, Point q1, Point p2, Point q2)  
    { 
        // Find the four orientations needed for  
        // general and special cases 
        int o1 = orientation(p1, q1, p2); 
        int o2 = orientation(p1, q1, q2); 
        int o3 = orientation(p2, q2, p1); 
        int o4 = orientation(p2, q2, q1); 
  
        // General case 
        if (o1 != o2 && o3 != o4) 
        { 
            return true; 
        } 
  
        // Special Cases 
        // p1, q1 and p2 are colinear and 
        // p2 lies on segment p1q1 
        if (o1 == 0 && onSegment(p1, p2, q1))  
        { 
            return true; 
        } 
  
        // p1, q1 and p2 are colinear and 
        // q2 lies on segment p1q1 
        if (o2 == 0 && onSegment(p1, q2, q1))  
        { 
            return true; 
        } 
  
        // p2, q2 and p1 are colinear and 
        // p1 lies on segment p2q2 
        if (o3 == 0 && onSegment(p2, p1, q2)) 
        { 
            return true; 
        } 
  
        // p2, q2 and q1 are colinear and 
        // q1 lies on segment p2q2 
        if (o4 == 0 && onSegment(p2, q1, q2)) 
        { 
            return true; 
        } 
  
        // Doesn't fall in any of the above cases 
        return false;  
    } 
  
    // Returns true if the point p lies  
    // inside the polygon[] with n vertices 
    public static boolean isInside(Point polygon[], int n, Point p) 
    { 
        // There must be at least 3 vertices in polygon[] 
        if (n < 3)  
        { 
            return false; 
        } 
  
        // Create a point for line segment from p to infinite 
        Point extreme = new Point(INF, p.y); 
  
        // Count intersections of the above line  
        // with sides of polygon 
        int count = 0, i = 0; 
        do 
        { 
            int next = (i + 1) % n; 
  
            // Check if the line segment from 'p' to  
            // 'extreme' intersects with the line  
            // segment from 'polygon[i]' to 'polygon[next]' 
            if (doIntersect(polygon[i], polygon[next], p, extreme))  
            { 
                // If the point 'p' is colinear with line  
                // segment 'i-next', then check if it lies  
                // on segment. If it lies, return true, otherwise false 
                if (orientation(polygon[i], p, polygon[next]) == 0) 
                { 
                    return onSegment(polygon[i], p, 
                                     polygon[next]); 
                } 
  
                count++; 
            } 
            i = next; 
        } while (i != 0); 
  
        // Return true if count is odd, false otherwise 
        return (count % 2 == 1); // Same as (count%2 == 1) 
    } 
  
    //Given a line defined by 2 points, return 
    //the two points perpendicular to tis line 
    // at the distance from p2 equal to the distance from 
    // p1 to p2
    public static Point[] getPerpendicularPoints(Point p1, Point p2) {

    	double x1, x2, y1, y2, x3=0, y3=0, x4=0, y4=0;
    	x1 = p1.x;
    	y1 = p1.y;
    	x2 = p2.x;
    	y2 = p2.y;
    	    	
    	double lengthSq = Point2D.distanceSq(x1, y1, x2, y2);
    	double grad = 0.0;
    	if((Double.compare((y2-y1), 0) != 0) && (Double.compare((x2-x1), 0) != 0))
    		grad = (y2-y1)/(x2-x1);
    	
    	x3 = x2 + (Math.sqrt((lengthSq / (1 + (1/Math.pow(grad,2))))));
    	y3 = (-1/grad)*(x3 - x2) + y2;
    	Point corner1 = new Point(x3, y3);
    	
    	x4 = x2 - (Math.sqrt((lengthSq / (1 + (1/Math.pow(grad,2))))));
    	y4 = (-1/grad)*(x4 - x2) + y2;
    	Point corner2 = new Point(x4, y4);
    	
    	return new Point[]{corner1, corner2};
    }

	public static Point getMidpoint(Point newStart, Point newEnd) {
		
		double dx = newEnd.x - newStart.x;
		double dy = newEnd.y - newStart.y;
		
		return new Point(newStart.x + (dx/2), newStart.y +(dy/2));
	}

	public static Point extendLine(Point originalStart, Point originalEnd) {
		
		double x1, x2, y1, y2;
		x1 = originalStart.x;
    	y1 = originalStart.y;
    	x2 = originalEnd.x;
    	y2 = originalEnd.y;
    	
    	double length = Point2D.distance(originalStart.x, originalStart.y, originalEnd.x, originalEnd.y);
    	double grad = 0.0;
    	if((Double.compare((y2-y1), 0) != 0) && (Double.compare((x2-x1), 0) != 0))
    		grad = (y2-y1)/(x2-x1);
    	
		length = length*((double)EXTEND_PERCENTAGE/100d);
		double lengthSq = Math.pow(length, 2);
				
		double x3Plus = x2 + Math.sqrt(lengthSq/(1+grad)); 
		double x3Minus = x2- (Math.sqrt(lengthSq/(1+grad)));
		
		double y3Plus = y2 + length/Math.sqrt(2d);
		double y3Minus = y2 - (length/Math.sqrt(2d));
		
		Point p3 = choosePlusOrMinus(x3Plus, x3Minus, y3Plus, y3Minus, originalStart, originalEnd);
		
		return p3;
	}

	private static Point choosePlusOrMinus(double x3Plus, double x3Minus, double y3Plus, double y3Minus, Point originalStart, Point originalEnd) {
		
		double x1, x2, y1, y2;
		x1 = originalStart.x;
    	y1 = originalStart.y;
    	x2 = originalEnd.x;
    	y2 = originalEnd.y;
    	
    	double x3Pos = Double.max(x3Plus, x3Minus);
    	double x3Neg = Double.min(x3Plus, x3Minus);
    	
    	double y3Pos = Double.max(y3Plus, y3Minus);
    	double y3Neg = Double.min(y3Plus, y3Minus);  	
    	
		if(x1 < x2 && y1 < y2) {
			//normal +vs
			//choose plus x and y
			return new Point(x3Pos, y3Pos);
		}
		else if(x1 < x2 && y1 > y2) {
			//normal -ve
			//choose plus x and neg y
			return new Point(x3Pos, y3Neg);
		}
		else if(x1 > x2 && y1 < y2) {
			//reverse -ve
			//choose neg
			return new Point(x3Neg, y3Pos);
		}
		else {
			//reverse +ve
			//choose neg
			return new Point(x3Neg, y3Neg);
		}		
	}


}





